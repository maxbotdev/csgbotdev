package com.mli.bot.csg.response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.utils.CommonUtility;
@Service
public class ThankYouResponse {
	@Autowired
	private CommonUtility commonUtility;
	
	String speech="";
	public String thankyouResponse(String speech)
	{
		StringBuilder sb = commonUtility.getButtonsResponse(speech);
		StringBuilder sb3 = commonUtility.addFinalRespone();
		sb.append(sb3.toString());
		return sb.toString();
	}
}
