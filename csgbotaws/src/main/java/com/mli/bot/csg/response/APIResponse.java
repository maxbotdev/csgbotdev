package com.mli.bot.csg.response;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.utils.CommonUtility;

@Service
public class APIResponse
{
	@Autowired
	private CommonUtility commonUtility;
	
	String speech="";
	public String apiResponse(Map<String,Map<String,String>> externalMap, String sessionId, String speech)
	{
		StringBuilder sb = commonUtility.getButtonsResponse(speech);

		StringBuilder sb2=getDyanamicResponse(externalMap,sessionId);
		
		sb.append(sb2.toString());
		StringBuilder sb3 = commonUtility.addFinalRespone();
		sb.append(sb3.toString());
		return sb.toString();
	}
	
	
	private StringBuilder getDyanamicResponse(Map<String, Map<String, String>> externalMap, String sessionId) {
		StringBuilder sb2= new StringBuilder();
		Map<String, String> newMap = externalMap.get(sessionId);
		int size = newMap.size();
		int count=0;
		for (Map.Entry<String, String> entry : newMap.entrySet())
		{
			count++;
			String key = entry.getKey();
			String value = entry.getValue();

			sb2.append("	        {	");
			sb2.append("	          \"text\": \" "+value+"\",	");
			sb2.append("	          \"postback\": \""+key+"\",	");
			sb2.append("	          \"link\": \"\"	");
			if(count!=size)
			{
				sb2.append("	        },	");
			}
			else
			{
				sb2.append("	        }	");
			}
		}
		
		return  sb2;
	}
}
