package com.mli.bot.csg.beans;

import org.springframework.stereotype.Component;

@Component
public class ArcisServicesApiBean
{
    private String campaignName;
    private String sourceNumber;
    private String subsourceNumber;
    private String name;
    private String policyNumber;
    private String registeredMobileNo;
    private String email;
    private String customerIntractionDate;
    private String age;
    private String income;
    private String l2BIName;
    private String l2BIAge;
    private String educationCost;
    private String investmentRiskPreference;
    private String qualificationStreamChosen;
    private String currentSavingAmount;
    private String monthlySaving;
    private String savingTillDate;
    private String currentHouseHoldExpence;
    private String expectedMonthlyExpenceAfterRetirement;
    private String premiumAmount;
    private String ageCustomerNeedMoney;
    private String expectedMaturityAmount;
    private String productRecommended;
    private String currentLoanAmount;
    private String savingsPlannedToDo;
    private String lifeInsuranceCover;
    private String protectionCoverNeeded;
    private String additionalCoverNeeded;
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public String getSourceNumber() {
		return sourceNumber;
	}
	public void setSourceNumber(String sourceNumber) {
		this.sourceNumber = sourceNumber;
	}
	
	public String getSubsourceNumber() {
		return subsourceNumber;
	}
	public void setSubsourceNumber(String subsourceNumber) {
		this.subsourceNumber = subsourceNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getRegisteredMobileNo() {
		return registeredMobileNo;
	}
	public void setRegisteredMobileNo(String registeredMobileNo) {
		this.registeredMobileNo = registeredMobileNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCustomerIntractionDate() {
		return customerIntractionDate;
	}
	public void setCustomerIntractionDate(String customerIntractionDate) {
		this.customerIntractionDate = customerIntractionDate;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getIncome() {
		return income;
	}
	public void setIncome(String income) {
		this.income = income;
	}
	public String getL2BIName() {
		return l2BIName;
	}
	public void setL2BIName(String l2biName) {
		l2BIName = l2biName;
	}
	public String getL2BIAge() {
		return l2BIAge;
	}
	public void setL2BIAge(String l2biAge) {
		l2BIAge = l2biAge;
	}
	public String getEducationCost() {
		return educationCost;
	}
	public void setEducationCost(String educationCost) {
		this.educationCost = educationCost;
	}
	public String getInvestmentRiskPreference() {
		return investmentRiskPreference;
	}
	public void setInvestmentRiskPreference(String investmentRiskPreference) {
		this.investmentRiskPreference = investmentRiskPreference;
	}
	public String getQualificationStreamChosen() {
		return qualificationStreamChosen;
	}
	public void setQualificationStreamChosen(String qualificationStreamChosen) {
		this.qualificationStreamChosen = qualificationStreamChosen;
	}
	public String getCurrentSavingAmount() {
		return currentSavingAmount;
	}
	public void setCurrentSavingAmount(String currentSavingAmount) {
		this.currentSavingAmount = currentSavingAmount;
	}
	public String getMonthlySaving() {
		return monthlySaving;
	}
	public void setMonthlySaving(String monthlySaving) {
		this.monthlySaving = monthlySaving;
	}
	public String getSavingTillDate() {
		return savingTillDate;
	}
	public void setSavingTillDate(String savingTillDate) {
		this.savingTillDate = savingTillDate;
	}
	public String getCurrentHouseHoldExpence() {
		return currentHouseHoldExpence;
	}
	public void setCurrentHouseHoldExpence(String currentHouseHoldExpence) {
		this.currentHouseHoldExpence = currentHouseHoldExpence;
	}
	public String getExpectedMonthlyExpenceAfterRetirement() {
		return expectedMonthlyExpenceAfterRetirement;
	}
	public void setExpectedMonthlyExpenceAfterRetirement(String expectedMonthlyExpenceAfterRetirement) {
		this.expectedMonthlyExpenceAfterRetirement = expectedMonthlyExpenceAfterRetirement;
	}
	public String getPremiumAmount() {
		return premiumAmount;
	}
	public void setPremiumAmount(String premiumAmount) {
		this.premiumAmount = premiumAmount;
	}
	public String getAgeCustomerNeedMoney() {
		return ageCustomerNeedMoney;
	}
	public void setAgeCustomerNeedMoney(String ageCustomerNeedMoney) {
		this.ageCustomerNeedMoney = ageCustomerNeedMoney;
	}
	public String getExpectedMaturityAmount() {
		return expectedMaturityAmount;
	}
	public void setExpectedMaturityAmount(String expectedMaturityAmount) {
		this.expectedMaturityAmount = expectedMaturityAmount;
	}
	public String getProductRecommended() {
		return productRecommended;
	}
	public void setProductRecommended(String productRecommended) {
		this.productRecommended = productRecommended;
	}
	public String getCurrentLoanAmount() {
		return currentLoanAmount;
	}
	public void setCurrentLoanAmount(String currentLoanAmount) {
		this.currentLoanAmount = currentLoanAmount;
	}
	public String getSavingsPlannedToDo() {
		return savingsPlannedToDo;
	}
	public void setSavingsPlannedToDo(String savingsPlannedToDo) {
		this.savingsPlannedToDo = savingsPlannedToDo;
	}
	public String getLifeInsuranceCover() {
		return lifeInsuranceCover;
	}
	public void setLifeInsuranceCover(String lifeInsuranceCover) {
		this.lifeInsuranceCover = lifeInsuranceCover;
	}
	public String getProtectionCoverNeeded() {
		return protectionCoverNeeded;
	}
	public void setProtectionCoverNeeded(String protectionCoverNeeded) {
		this.protectionCoverNeeded = protectionCoverNeeded;
	}
	public String getAdditionalCoverNeeded() {
		return additionalCoverNeeded;
	}
	public void setAdditionalCoverNeeded(String additionalCoverNeeded) {
		this.additionalCoverNeeded = additionalCoverNeeded;
	}
	@Override
	public String toString() {
		return "ArcisServicesApiBean [campaignName=" + campaignName + ", sourceNumber=" + sourceNumber
				+ ", subsourceNumber=" + subsourceNumber + ", name=" + name + ", policyNumber=" + policyNumber
				+ ", registeredMobileNo=" + registeredMobileNo + ", email=" + email + ", customerIntractionDate="
				+ customerIntractionDate + ", age=" + age + ", income=" + income + ", l2BIName=" + l2BIName
				+ ", l2BIAge=" + l2BIAge + ", educationCost=" + educationCost + ", investmentRiskPreference="
				+ investmentRiskPreference + ", qualificationStreamChosen=" + qualificationStreamChosen
				+ ", currentSavingAmount=" + currentSavingAmount + ", monthlySaving=" + monthlySaving
				+ ", savingTillDate=" + savingTillDate + ", currentHouseHoldExpence=" + currentHouseHoldExpence
				+ ", expectedMonthlyExpenceAfterRetirement=" + expectedMonthlyExpenceAfterRetirement
				+ ", premiumAmount=" + premiumAmount + ", ageCustomerNeedMoney=" + ageCustomerNeedMoney
				+ ", expectedMaturityAmount=" + expectedMaturityAmount + ", productRecommended=" + productRecommended
				+ ", currentLoanAmount=" + currentLoanAmount + ", savingsPlannedToDo=" + savingsPlannedToDo
				+ ", lifeInsuranceCover=" + lifeInsuranceCover + ", protectionCoverNeeded=" + protectionCoverNeeded
				+ ", additionalCoverNeeded=" + additionalCoverNeeded + "]";
	}
}