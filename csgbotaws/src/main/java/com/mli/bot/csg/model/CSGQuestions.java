package com.mli.bot.csg.model;

import java.util.Map;

public interface CSGQuestions 
{
	public String csgQuestions(Map<String,Map<String,String>> externalMap, String sessionId, String action);

}
