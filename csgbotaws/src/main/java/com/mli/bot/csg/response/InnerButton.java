package com.mli.bot.csg.response;

import java.io.Serializable;

public class InnerButton implements Serializable  {

private static final long serialVersionUID = 1L;
	
	String text;
	String postback;
	String link;
	
	/**
	 * default constructor
	 */
	public InnerButton() {
		super();
	}

	/**
	 * @param text
	 * @param postback
	 * @param link
	 */
	public InnerButton(String text, String postback, String link) {
		super();
		this.text = text;
		this.postback = postback;
		this.link = link;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPostback() {
		return postback;
	}

	public void setPostback(String postback) {
		this.postback = postback;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public String toString() {
		return "InnerButton [text=" + text + ", postback=" + postback + ", link=" + link + "]";
	}
}
