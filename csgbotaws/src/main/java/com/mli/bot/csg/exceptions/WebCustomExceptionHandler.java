package com.mli.bot.csg.exceptions;

import com.mli.bot.csg.response.GenericResponse;
import com.mli.bot.csg.response.WebhookResponse;

public class WebCustomExceptionHandler extends GenericCustomException 
{  
	   
	   private static final long serialVersionUID = 7397479533721583476L;
	   
	   public WebCustomExceptionHandler(GenericCustomException genericException)
	   {
		    super(genericException.getSpeech(),genericException.getDisplayText(),genericException.getData());
	   }
	   
	   public GenericResponse getFormattedExceptionResponse() 
	   {
		    return new WebhookResponse(speech, displayText, data);
	   }
	
}
