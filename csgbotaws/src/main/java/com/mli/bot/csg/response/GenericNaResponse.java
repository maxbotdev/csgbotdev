package com.mli.bot.csg.response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.utils.CommonUtility;

@Service
public class GenericNaResponse 
{
	@Autowired
	private CommonUtility commonUtility;
	
	String speech="";
	public String genericResponse( String speech)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"fulfillmentText\": \"displayed&spoken response\",	");
		sb.append("	  \"fulfillmentMessages\": [	");
		sb.append("	    {	");
		sb.append("	      \"text\": {	");
		sb.append("	        \"text\": [\""+speech+"\"]	");
		sb.append("	      }	");
		sb.append("	    }	");
		sb.append("	  ]	");
		sb.append("	 }	");
		
		return sb.toString();
	}
}
