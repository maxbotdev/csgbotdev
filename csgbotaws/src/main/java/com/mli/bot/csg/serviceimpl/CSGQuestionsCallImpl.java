package com.mli.bot.csg.serviceimpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mli.bot.csg.commons.BeanProperty;
import com.mli.bot.csg.service.CSGQuestionsCall;

@Service
public class CSGQuestionsCallImpl implements CSGQuestionsCall 
{
	
	@Autowired
	private BeanProperty bean;
	String questionsResponse;
	@Override
	public String csgQuestionsServiceCall(Map<String,Map<String,String>> externalMap, String sessionId, String action) 
	{
		String url=bean.getGetHappinessAndWorryFactors();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"type\":\""+action+"\"	");
		sb.append("	}	");

		HttpEntity<String> entity=new HttpEntity(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			questionsResponse = response.getBody();

		}
		return questionsResponse;
	}
}
