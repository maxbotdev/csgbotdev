package com.mli.bot.csg.service;

import java.util.Map;

public interface UserQuestionsCall 
{
	public String userQuestionsCall(Map<String, Map<String, String>> externalMap, String sessionId, String action, String questionsId);

}
