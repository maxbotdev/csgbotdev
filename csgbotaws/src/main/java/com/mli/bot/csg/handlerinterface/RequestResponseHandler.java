package com.mli.bot.csg.handlerinterface;

import com.mli.bot.csg.exceptions.GenericCustomException;
import com.mli.bot.csg.response.GenericResponse;

public interface RequestResponseHandler {

public void requestHandler() throws GenericCustomException ;
	
	public void responseHandler() throws GenericCustomException ;
	
	public GenericResponse getResponse() ;
	
	public void processRequest() throws GenericCustomException ;
}
