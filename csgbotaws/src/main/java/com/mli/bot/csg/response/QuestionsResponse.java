package com.mli.bot.csg.response;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.utils.CommonUtility;

@Service
public class QuestionsResponse 
{
	private static String filmMaking;
	private static String medical;
	static 
	{
		filmMaking = "FilmMaking";
		medical = "Medical";
	}
	
	@Autowired
	private CommonUtility commonUtility;
	
	String speech="";
	public String questionsResponse(String sessionId, String speech, String action, Map<String,Map<String,String>> questionAswerMap)
	{
		String speeches=speech.replaceAll("[-+^/]\"]*", "");
		speech=speeches.replaceAll("\"", "");
		StringBuilder sb;

		if("Q3".equalsIgnoreCase(action))
		{
			sb=q3Response(speech);
			
		}
		else if("Q4".equalsIgnoreCase(action) && "PG".equalsIgnoreCase(questionAswerMap.get(sessionId).get("Q3")+""))
		{
			sb=q4WithPG(speech);
		}
		else if("Q4".equalsIgnoreCase(action) && "UG".equalsIgnoreCase(questionAswerMap.get(sessionId).get("Q3")+""))
		{
			sb=q4WithUG(speech);
		}
		else if("Q5".equalsIgnoreCase(action) && !filmMaking.equalsIgnoreCase(questionAswerMap.get(sessionId).get("Q4")))
		{
			sb=q5WithOutFilmMaking(speech);
		}
		else if("Q5".equalsIgnoreCase(action) && filmMaking.equalsIgnoreCase(questionAswerMap.get(sessionId).get("Q4")))
		{
			sb=q5WithFilmMaking(speech);
		}
		else if("Q26".equalsIgnoreCase(action))
		{
			sb=q26Response(speech);
		}
		else
		{
		 sb=commonUtility.responseWithoutButton(speech);
		}
		return sb.toString();
	}
	private StringBuilder q26Response(String speech) {
		StringBuilder sb = commonUtility.getButtonsResponse(speech);
		sb.append(commonUtility.makeDyanamicButton("Low", "Low"));
		sb.append(commonUtility.makeDyanamicButton("Medium", "Medium"));
		sb.append(commonUtility.makeDyanamicButton("High", "High"));
		sb=commonUtility.removeLastCharcter(sb);
		sb.append(commonUtility.addFinalRespone());
		return sb;
	}
	private StringBuilder q5WithFilmMaking(String speech) {
		StringBuilder sb = commonUtility.getButtonsResponse(speech);
		sb.append(filmMakingResponse());
		sb=commonUtility.removeLastCharcter(sb);
		sb.append(commonUtility.addFinalRespone());		
		return sb;
	}
	private StringBuilder filmMakingResponse() {
		StringBuilder sb=new StringBuilder();
		sb.append(commonUtility.makeDyanamicButton("India", "India"));
		sb.append(commonUtility.makeDyanamicButton("US", "US"));
		sb.append(commonUtility.makeDyanamicButton("UK", "UK"));
		sb.append(commonUtility.makeDyanamicButton("Australia", "Australia"));
		return sb;
	}
	private StringBuilder q5WithOutFilmMaking(String speech) {
		StringBuilder sb = commonUtility.getButtonsResponse(speech);
		sb.append(filmMakingResponse());
		sb.append(commonUtility.makeDyanamicButton("Singapore", "Singapore"));
		sb=commonUtility.removeLastCharcter(sb);
		sb.append(commonUtility.addFinalRespone());	
		return sb;
	}
	private StringBuilder q4WithUG(String speech) {
		StringBuilder sb = commonUtility.getButtonsResponse(speech);
		sb.append(commonUtility.makeDyanamicButton("Arts", "Arts"));
		sb.append(commonUtility.makeDyanamicButton("Science", "Science"));
		sb.append(commonUtility.makeDyanamicButton("Commerce", "Commerce"));
		sb.append(commonUtility.makeDyanamicButton("Law", "Law"));
		sb.append(commonUtility.makeDyanamicButton("Engineering", "Engineering"));
		sb.append(commonUtility.makeDyanamicButton("Fashion", "Fashion"));
		sb.append(commonUtility.makeDyanamicButton(medical, medical));
		sb=commonUtility.removeLastCharcter(sb);
		sb.append(commonUtility.addFinalRespone());
		return sb;
	}
	private StringBuilder q4WithPG(String speech) {
		StringBuilder sb = commonUtility.getButtonsResponse(speech);
		sb.append(commonUtility.makeDyanamicButton("Management", "Management"));
		sb.append(commonUtility.makeDyanamicButton(medical, medical));
		sb.append(commonUtility.makeDyanamicButton("MTech/Msc", "MTechMsc"));
		sb.append(commonUtility.makeDyanamicButton(filmMaking+"(2Year)", "FilmMaking"));
		sb=commonUtility.removeLastCharcter(sb);
		sb.append(commonUtility.addFinalRespone());
		return sb;
	}
	private StringBuilder q3Response(String speech) {
		StringBuilder sb =commonUtility.getButtonsResponse(speech);
		sb.append(commonUtility.makeDyanamicButton("PostGraduation", "PG"));
		sb.append(commonUtility.makeDyanamicButton("Graduation", "UG"));
		sb=commonUtility.removeLastCharcter(sb);
		sb.append(commonUtility.addFinalRespone());
		return sb;
	}
}
