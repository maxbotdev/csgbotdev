package com.mli.bot.csg.response;

import java.io.Serializable;

public class WebhookResponsefulfillmentMsg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	WebhookResponseText text;
 
	public WebhookResponsefulfillmentMsg()
	{
		text=new WebhookResponseText();
	}
	
	
	public WebhookResponseText getText() {
		return text;
	}

	public void setText(WebhookResponseText text) {
		this.text = text;
	}

	
}
