package com.mli.bot.csg.response;

import java.io.Serializable;
import java.util.Arrays;

public class WebhookResponseText implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String[]  text;
	
	public WebhookResponseText()
	{
		text=new String[1];
	}

	public String[] getText() {
		return text;
	}

	public void setText(String[] text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "WebhookResponseText [text=" + Arrays.toString(text) + "]";
	}
	
}
