package com.mli.bot.csg.calculator;

import java.util.Map;

public interface RegularSavings 
{
	public String regularSavings(Map<String,Map<String,String>> questionAswerMap, String sessionId);
}
