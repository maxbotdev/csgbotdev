package com.mli.bot.csg.service;

public interface ChildMarriageCalculator 
{
	public String childMarriageCalculator(String age, String marriageAge, String neededMoney, String asideMoney, 
			String expectSaving,String inflationRate);

}
