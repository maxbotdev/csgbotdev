package com.mli.bot.csg.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WebhookRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6822381422118405229L;
	String responseId;
	String session;
	WebhookRequestQueryResult queryResult;
	
	public String getResponseId() {
		return responseId;
	}
	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public WebhookRequestQueryResult getQueryResult() {
		return queryResult;
	}
	public void setQueryResult(WebhookRequestQueryResult queryResult) {
		this.queryResult = queryResult;
	}

}
