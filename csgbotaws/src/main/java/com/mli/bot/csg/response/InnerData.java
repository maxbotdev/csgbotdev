package com.mli.bot.csg.response;

import java.io.Serializable;


public class InnerData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Facebook facebook;

	/**
	 * default constructor
	 */
	public InnerData() {
		super();
	}

	/**
	 * @param facebook
	 */
	public InnerData(Facebook facebook) {
		super();
		this.facebook = facebook;
	}

	public Facebook getFacebook() {
		return facebook;
	}

	public void setFacebook(Facebook facebook) {
		this.facebook = facebook;
	}

	@Override
	public String toString() {
		return "InnerData [facebook=" + facebook + "]";
	}


}
