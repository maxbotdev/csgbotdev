package com.mli.bot.csg.serviceimpl;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfCopyFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;
import com.mli.bot.csg.beans.BasicDetailQuesAnsBean;
import com.mli.bot.csg.beans.FinalPlanDetailBean;
import com.mli.bot.csg.beans.PDFQuestionBean;
import com.mli.bot.csg.commons.BeanProperty;
import com.mli.bot.csg.commons.CalculateTimeDiff;
import com.mli.bot.csg.service.CommonService;
import com.mli.bot.csg.service.PDFCreationService;



/**
 * @author Adit
 *
 */
@Service
public class PDFCreationServiceImpl implements PDFCreationService {

	private static Logger logger;
	private static final Color BASIC_DETAILS_COLOR;
	private static final Color PDF_CONTENT_COLOR;
	private static final Color DARK_ORANGE;
	private static final Color WHITE_COLOR;
	private static String protectionOfMyFamily;
	private static String yourInvestmentRisk;
	private static String year;
	private static String childMarriage;
	private static String retirement;
	
	static
	{
		logger = LogManager.getLogger(PDFCreationServiceImpl.class);
		BASIC_DETAILS_COLOR = new Color(102, 102, 102);
		PDF_CONTENT_COLOR = new Color(93, 94, 94);
		DARK_ORANGE = new Color(243, 121, 33);
		WHITE_COLOR = new Color(255, 255, 255);
		protectionOfMyFamily="Protection Of My Family";
		yourInvestmentRisk="Your Investment Risk Preference:";
		year=" Year";
		childMarriage="Child Marriage";
		retirement="Retirement";
		
	}

	@Autowired
	private CommonService commonService;

	@Autowired
	private BeanProperty bean;

	@Autowired
	private BasicDetailQuesAnsBean basicDetailQuesAnsBean;

	@Autowired
	PDFQuestionBean pDFQuestionBean;

	@Autowired
	private FinalPlanDetailBean finalPlanDetailBean;

	@Autowired
	CalculateTimeDiff calculateTimeDiff;
	
	
	public void createPdf(String username, String pathwithName, Map externalMap, String sessionId, String planName)
	{
		
		logger.info("start createPdf ....for PDF");
		OutputStream writer = null;
		PdfStamper stamper=null;
		try {
			
			ByteArrayOutputStream baos11 = null;
			writer = new FileOutputStream(pathwithName);
			ByteArrayOutputStream buffer11 = new ByteArrayOutputStream();

			baos11 = textPdfCreator(username, externalMap, sessionId, planName);
			PdfReader reader11 = new PdfReader(baos11.toByteArray());
			PdfCopyFields copy11 = new PdfCopyFields(buffer11);
			copy11.addDocument(reader11);
			copy11.close();
			PdfReader reader11a = new PdfReader(buffer11.toByteArray());
			 stamper = new PdfStamper(reader11a, writer);
			 writer.flush();
			 Map s3Map=new HashMap();
			 s3Map.put("s3ImageName",planName);
			 externalMap.put(sessionId+"S3",s3Map);
			
		} catch (Exception ex) {
			logger.info("Exception in createpdf method :: " + ex);
		} finally {
			
				try {
					if(stamper!=null)
					{
					  stamper.close();
					}
					if (writer != null)
					{
					  writer.close();
					}
					
				} catch (Exception e)
				{
				  logger.info("creating exception while closing writer object " + e);
				}
		}
		logger.info("End main ....for PDF ");
	}

	

	public ByteArrayOutputStream textPdfCreator(String username, Map externalMap, String sessionId,
			String planNameForImage) {
		logger.info("start executing method TextPdfcreator ...");
		Map<String, String> plan = (Map) externalMap.get(sessionId);
		Map detailsMap = (Map) externalMap.get(sessionId + "basicDetailsMap");
		String term = (String) detailsMap.get(sessionId + "term");
		
		PDFQuestionBean pdfQuestionBean = getQuestions(detailsMap, sessionId);
		FinalPlanDetailBean finalPlanDetailBean = finalPlanDetail(plan);
		
		String planName;
		String planSaving;
		String planTargetYrs;
		String planGap;
		String planTarget;
		String planSavingAns = "";
		String planTargetYrsAns = "";
		String planGapAns = "";
		String planTargetAns = "";

		ByteArrayOutputStream retrunOutput = null;
		PdfWriter docWriter = null;
		Image genricImage = null;
		Image protectionImage = null;
		Image backgroundImage = null;
		Image customImage = null;
		Image customProtectionImage = null;
		Image imageGraph = null;
		Image dyanamicImage = null;
		Document doc = new Document();
		String rs = "Rs ";
		try {
			String workingDir = bean.getGetPdfPath();

			BaseFont sfproMediumFont = BaseFont.createFont(workingDir + File.separator + "SF-Pro-Medium.ttf",
					BaseFont.WINANSI, true);
			BaseFont sfproBoldFont = BaseFont.createFont(workingDir + File.separator + "SF-Pro-Bold.ttf",
					BaseFont.WINANSI, true);

			backgroundImage = Image.getInstance(workingDir + File.separator + "backgroundImage.jpg");
			backgroundImage.scalePercent(48.0F);
			backgroundImage.setAbsolutePosition(0.0F, 0.0F);
			backgroundImage.scaleAbsoluteWidth(595.0F);

			BaseFont rupeesSymbol = BaseFont.createFont(workingDir + File.separator + "WebRupee.V2.0.ttf", "Cp1252",
					true);

			retrunOutput = new ByteArrayOutputStream();
			doc = new Document(PageSize.A4);
			docWriter = PdfWriter.getInstance(doc, retrunOutput);
			doc.open();

			PdfContentByte contentByte = docWriter.getDirectContent();
			ColumnText coltext = new ColumnText(contentByte);
			Phrase pagecontent = null;

			String header = "Financial Planning Summary";
			String customerDeclarationFirst = "Dear " + commonService.camelCase(pdfQuestionBean.getQ27()) + ",";
			String customerDeclarationSecond = "Greetings from Max Life Insurance!";
			String customerDeclarationThird = "Basis the information shared by you, we bring to you complete analysis to help plan for financial needs efficiently";
			String customerDeclarationFour = "and secure the future for your family.";

			pagecontent = new Phrase(header, new Font(sfproBoldFont, 24.F, 0, new Color(128, 130, 133)));
			coltext.setSimpleColumn(pagecontent, 160.0F, 0.0F, 570.0F, 800.0F, 11.0F, 0);
			coltext.go();

			pagecontent = new Phrase(customerDeclarationFirst, new Font(sfproMediumFont, 10.0F, 0, PDF_CONTENT_COLOR));
			coltext.setSimpleColumn(pagecontent, 30.0F, 0.0F, 580.0F, 740.0F, 10.0F, 0);
			coltext.go();

			pagecontent = new Phrase(customerDeclarationSecond, new Font(sfproMediumFont, 10.0F, 0, PDF_CONTENT_COLOR));
			coltext.setSimpleColumn(pagecontent, 30.0F, 0.0F, 580.0F, 725.0F, 10.0F, 0);
			coltext.go();

			pagecontent = new Phrase(customerDeclarationThird, new Font(sfproMediumFont, 10.F, 0, PDF_CONTENT_COLOR));
			coltext.setSimpleColumn(pagecontent, 30.0F, 0.0F, 580.0F, 710.0F, 10.0F, 0);
			coltext.go();

			pagecontent = new Phrase(customerDeclarationFour, new Font(sfproMediumFont, 10.0F, 0, PDF_CONTENT_COLOR));
			coltext.setSimpleColumn(pagecontent, 30.0F, 0.0F, 580.0F, 695.0F, 10.0F, 0);
			coltext.go();

			
			basicDetailQuesAnsBean.setBasicDetail("Basic details");
			basicDetailQuesAnsBean.setBasicDetailQuesFirst("Your Age:");
			basicDetailQuesAnsBean.setBasicDetailQuesSecond("Your current Annual Salary:");

			if (protectionOfMyFamily.equalsIgnoreCase(finalPlanDetailBean.getBasicDetails()))
			{
				basicDetailQuesAnsBean.setBasicDetailQuesThird("Current Household expenses:");
				basicDetailQuesAnsBean.setBasicDetailQuesFour(" Current loan amount:");
				basicDetailQuesAnsBean.setBasicDetailQuesFive(" Savings done till date:");
				basicDetailQuesAnsBean.setBasicDetailQuesSix(" Savings planned to do:");
				basicDetailQuesAnsBean.setBasicDetailQuesSeven(" Life insurance cover:");

				basicDetailQuesAnsBean.setBasicDetailAnsFirst(pdfQuestionBean.getQ13() + year);
				basicDetailQuesAnsBean
						.setBasicDetailAnsSecond(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ15()));
				basicDetailQuesAnsBean
						.setBasicDetailAnsThird(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ25()));
				basicDetailQuesAnsBean
						.setBasicDetailAnsFour(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ17()));
				basicDetailQuesAnsBean
						.setBasicDetailAnsFive(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ16()));
				basicDetailQuesAnsBean
						.setBasicDetailAnsSix(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ14()));
				basicDetailQuesAnsBean
						.setBasicDetailAnsSeven(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ18()));
				
				
								Paragraph ansSecond = new Paragraph();
								ansSecond.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
								ansSecond.add(new Chunk(basicDetailQuesAnsBean.getBasicDetailAnsThird(),
						        new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE)));
						coltext.setSimpleColumn(ansSecond, 190.0F, 0.0F, 580.0F, 593.0F, 10.0F, 0);
						coltext.go();
						
						
						Paragraph ansFour = new Paragraph();
						ansFour.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
						ansFour.add(new Chunk(basicDetailQuesAnsBean.getBasicDetailAnsFour(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE)));
						coltext.setSimpleColumn(ansFour, 190.0F, 0.0F, 580.0F, 575.0F, 10.0F, 0);
						coltext.go();
						
						
						
						
						Paragraph ansFiveProtection = new Paragraph();
						ansFiveProtection.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
						ansFiveProtection.add(new Chunk(basicDetailQuesAnsBean.getBasicDetailAnsFive(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE)));
						coltext.setSimpleColumn(ansFiveProtection, 190.0F, 0.0F, 580.0F, 557.0F, 10.0F, 0);
						coltext.go();
						
						Paragraph ansSixProtection = new Paragraph();
						ansSixProtection.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
						ansSixProtection.add(new Chunk(basicDetailQuesAnsBean.getBasicDetailAnsSix(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE)));
						coltext.setSimpleColumn(ansSixProtection, 190.0F, 0.0F, 580.0F, 539.0F, 10.0F, 0);
						coltext.go();
						
						
						
						
						Paragraph ansSeven = new Paragraph();
						ansSeven.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
						ansSeven.add(new Chunk(basicDetailQuesAnsBean.getBasicDetailAnsSeven(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE)));
						coltext.setSimpleColumn(ansSeven, 190.0F, 0.0F, 580.0F, 521.0F, 10.0F, 0);
						coltext.go();
						
			}
			else if ("Child Education".equalsIgnoreCase(finalPlanDetailBean.getBasicDetails()))
			{
				basicDetailQuesAnsBean.setBasicDetailQuesThird(pdfQuestionBean.getQ1() + " Age:");
				basicDetailQuesAnsBean.setBasicDetailQuesFour("Qualification and Stream chosen:");
				basicDetailQuesAnsBean.setBasicDetailQuesFive("Country Chosen:");
				basicDetailQuesAnsBean.setBasicDetailQuesSix("Your child's age when money is needed:");
				basicDetailQuesAnsBean.setBasicDetailQuesSeven(yourInvestmentRisk);

				basicDetailQuesAnsBean.setBasicDetailAnsFirst(pdfQuestionBean.getQ13() + year);
				basicDetailQuesAnsBean.setBasicDetailAnsSecond(pdfQuestionBean.getQ2() + year);
				basicDetailQuesAnsBean
						.setBasicDetailAnsThird(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ25()));
				basicDetailQuesAnsBean.setBasicDetailAnsFour(pdfQuestionBean.getQ3() + "-" + pdfQuestionBean.getQ4());
				basicDetailQuesAnsBean.setBasicDetailAnsFive(pdfQuestionBean.getQ5());
				basicDetailQuesAnsBean.setBasicDetailAnsSix(pdfQuestionBean.getQ7() + year);
				basicDetailQuesAnsBean.setBasicDetailAnsSeven(pdfQuestionBean.getQ26());
				
				
				        pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsSecond(), 
						new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
						coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 593.0F, 10.0F, 0);
						coltext.go();
						pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsFour(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
						coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 575.0F, 10.0F, 0);
						coltext.go();
						
						
						pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsFive(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
						coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 557.0F, 10.0F, 0);
						coltext.go();
						pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsSix(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
						coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 539.0F, 10.0F, 0);
						coltext.go();
						pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsSeven(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
						coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 521.0F, 10.0F, 0);
						coltext.go();
						

			}
			else if (childMarriage.equalsIgnoreCase(finalPlanDetailBean.getBasicDetails())) 
			{

				basicDetailQuesAnsBean.setBasicDetailQuesThird(pdfQuestionBean.getQ1() + " Age:");
				basicDetailQuesAnsBean
						.setBasicDetailQuesFour("Child's age when he will get married:");
				basicDetailQuesAnsBean.setBasicDetailQuesFive("Total money needed for the wedding:");
				basicDetailQuesAnsBean.setBasicDetailQuesSix("Savings done till date:");
				basicDetailQuesAnsBean.setBasicDetailQuesSeven(yourInvestmentRisk);

				basicDetailQuesAnsBean.setBasicDetailAnsFirst(pdfQuestionBean.getQ13() + year);
				basicDetailQuesAnsBean.setBasicDetailAnsSecond(pdfQuestionBean.getQ2() + year);
				basicDetailQuesAnsBean
						.setBasicDetailAnsThird(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ25()));
				basicDetailQuesAnsBean.setBasicDetailAnsFour(pdfQuestionBean.getQ10() + year);
				basicDetailQuesAnsBean
						.setBasicDetailAnsFive(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ11()));
				basicDetailQuesAnsBean
						.setBasicDetailAnsSix(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ12()));
				basicDetailQuesAnsBean.setBasicDetailAnsSeven(pdfQuestionBean.getQ26());
				
						pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsSecond(), 
						new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
						coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 593.0F, 10.0F, 0);
						coltext.go();
						pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsFour(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
						coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 575.0F, 10.0F, 0);
						coltext.go();
						
						
						
						
						Paragraph ansFiveChildMarriage = new Paragraph();
						ansFiveChildMarriage.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
						ansFiveChildMarriage.add(new Chunk(basicDetailQuesAnsBean.getBasicDetailAnsFive(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE)));
						coltext.setSimpleColumn(ansFiveChildMarriage, 190.0F, 0.0F, 580.0F, 557.0F, 10.0F, 0);
						coltext.go();
						
						Paragraph ansSixChildMarriage = new Paragraph();
						ansSixChildMarriage.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
						ansSixChildMarriage.add(new Chunk(basicDetailQuesAnsBean.getBasicDetailAnsSix(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE)));
						coltext.setSimpleColumn(ansSixChildMarriage, 190.0F, 0.0F, 580.0F, 539.0F, 10.0F, 0);
						coltext.go();
						
						pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsSeven(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
						coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 521.0F, 10.0F, 0);
						coltext.go();
				

			}
			else if (retirement.equalsIgnoreCase(finalPlanDetailBean.getBasicDetails()))
			{
				basicDetailQuesAnsBean.setBasicDetailQuesThird("Current Household expenses:");
				basicDetailQuesAnsBean.setBasicDetailQuesFour("Savings done till date:");
				basicDetailQuesAnsBean.setBasicDetailQuesFive("Expected Age of "+retirement+":");
				basicDetailQuesAnsBean.setBasicDetailQuesSix("Monthly expense post retirement est.:");
				basicDetailQuesAnsBean.setBasicDetailQuesSeven(yourInvestmentRisk);

				basicDetailQuesAnsBean.setBasicDetailAnsFirst(pdfQuestionBean.getQ13() + year);
				basicDetailQuesAnsBean
						.setBasicDetailAnsSecond(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ15()));
				basicDetailQuesAnsBean
						.setBasicDetailAnsThird(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ25()));
				basicDetailQuesAnsBean
						.setBasicDetailAnsFour(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ20()));
				basicDetailQuesAnsBean.setBasicDetailAnsFive(pdfQuestionBean.getQ19() + year);
				basicDetailQuesAnsBean
						.setBasicDetailAnsSix(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ24()));
				basicDetailQuesAnsBean.setBasicDetailAnsSeven(pdfQuestionBean.getQ26());
				
				
						Paragraph ansSecond = new Paragraph();
						ansSecond.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
						ansSecond.add(new Chunk(basicDetailQuesAnsBean.getBasicDetailAnsSecond(),
						new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE)));
						coltext.setSimpleColumn(ansSecond, 190.0F, 0.0F, 580.0F, 593.0F, 10.0F, 0);
						coltext.go();
						
						Paragraph ansFour = new Paragraph();
						ansFour.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
						ansFour.add(new Chunk(basicDetailQuesAnsBean.getBasicDetailAnsFour(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE)));
						coltext.setSimpleColumn(ansFour, 190.0F, 0.0F, 580.0F, 575.0F, 10.0F, 0);
						coltext.go();
						
						pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsFive(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
						coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 557.0F, 10.0F, 0);
						coltext.go();
						
						Paragraph ansSix = new Paragraph();
						ansSix.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
						ansSix.add(new Chunk(basicDetailQuesAnsBean.getBasicDetailAnsSix(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE)));
						coltext.setSimpleColumn(ansSix, 190.0F, 0.0F, 580.0F, 539.0F, 10.0F, 0);
						coltext.go();
						pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsSeven(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
						coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 521.0F, 10.0F, 0);
						coltext.go();
				

			} else if ("Regular Saving".equalsIgnoreCase(finalPlanDetailBean.getBasicDetails()))
			{
				basicDetailQuesAnsBean.setBasicDetailQuesThird("Target wealth to be created:");
				basicDetailQuesAnsBean.setBasicDetailQuesFour("Your age when you need this wealth:");
				basicDetailQuesAnsBean.setBasicDetailQuesFive("Savings done, till date:");
				basicDetailQuesAnsBean.setBasicDetailQuesSix("Number of years to goal:");
				basicDetailQuesAnsBean.setBasicDetailQuesSeven(yourInvestmentRisk);

				basicDetailQuesAnsBean.setBasicDetailAnsFirst(pdfQuestionBean.getQ13() + year);
				basicDetailQuesAnsBean
						.setBasicDetailAnsSecond(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ22()));
				basicDetailQuesAnsBean
						.setBasicDetailAnsThird(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ25()));
				basicDetailQuesAnsBean.setBasicDetailAnsFour(pdfQuestionBean.getQ21() + year);
				basicDetailQuesAnsBean
						.setBasicDetailAnsFive(commonService.commaSepratedIndianFormat(pdfQuestionBean.getQ23()));
				
				basicDetailQuesAnsBean.setBasicDetailAnsSix(pdfQuestionBean.getQ21() + "-" + pdfQuestionBean.getQ13());
				basicDetailQuesAnsBean.setBasicDetailAnsSeven(pdfQuestionBean.getQ26());
				
				
							Paragraph ansSeccond = new Paragraph();
							ansSeccond.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
							ansSeccond.add(new Chunk(basicDetailQuesAnsBean.getBasicDetailAnsSecond(),
						    new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE)));
						coltext.setSimpleColumn(ansSeccond, 190.0F, 0.0F, 580.0F, 593.0F, 10.0F, 0);
						coltext.go();
						pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsFour(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
						coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 575.0F, 10.0F, 0);
						coltext.go();
						
						
						
						Paragraph ansFive = new Paragraph();
						ansFive.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
						ansFive.add(new Chunk(basicDetailQuesAnsBean.getBasicDetailAnsFive(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE)));
						coltext.setSimpleColumn(ansFive, 190.0F, 0.0F, 580.0F, 557.0F, 10.0F, 0);
						coltext.go();
						pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsSix(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
						coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 539.0F, 10.0F, 0);
						coltext.go();
						pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsSeven(),
								new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
						coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 521.0F, 10.0F, 0);
						coltext.go();
			}

			if (protectionOfMyFamily.equalsIgnoreCase(finalPlanDetailBean.getBasicDetails()))
			{
				planName = protectionOfMyFamily;
				planSaving = "Protection cover needed";
				planTargetYrs = "Life cover till date";
				planGap = "Gap in achieving";
				planTarget = "Monthly Payout";
				planSavingAns = commonService.commaSepratedIndianFormat(finalPlanDetailBean.getGap());
				planTargetYrsAns = commonService.commaSepratedIndianFormat(finalPlanDetailBean.getTotalSaving());
				planGapAns = commonService.commaSepratedIndianFormat(finalPlanDetailBean.getSavingTillDate());
				planTargetAns = commonService.commaSepratedIndianFormat(finalPlanDetailBean.getMonthlyPayout());
			}
			else 
			{
				planName = finalPlanDetailBean.getBasicDetails();
				planSaving = "Total Saving Required ";
				planTargetYrs = "Saving Till Date";
				planGap = "Gap in achieving";
				planTarget = "Monthly Payout";
				planSavingAns = commonService.commaSepratedIndianFormat(finalPlanDetailBean.getGap());
				planTargetYrsAns = commonService.commaSepratedIndianFormat(finalPlanDetailBean.getTotalSaving());
				planGapAns = commonService.commaSepratedIndianFormat(finalPlanDetailBean.getSavingTillDate());
				planTargetAns = commonService.commaSepratedIndianFormat(finalPlanDetailBean.getMonthlyPayout());
			}


			pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetail(),
					new Font(sfproMediumFont, 12.0F, 0, new Color(35, 31, 32)));
			coltext.setSimpleColumn(pagecontent, 30.0F, 0.0F, 580.0F, 652.0F, 10.0F, 0);
			coltext.go();

			pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailQuesFirst(),
					new Font(sfproMediumFont, 9.0F, 0, BASIC_DETAILS_COLOR));
			coltext.setSimpleColumn(pagecontent, 34.0F, 0.0F, 580.0F, 630.0F, 10.0F, 0);
			coltext.go();

			pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailQuesSecond(),
					new Font(sfproMediumFont, 9.0F, 0, BASIC_DETAILS_COLOR));
			coltext.setSimpleColumn(pagecontent, 34.0F, 0.0F, 580.0F, 611.0F, 10.0F, 0);
			coltext.go();
			pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailQuesThird(),
					new Font(sfproMediumFont, 9.0F, 0, BASIC_DETAILS_COLOR));
			coltext.setSimpleColumn(pagecontent, 34.0F, 0.0F, 580.0F, 593.0F, 10.0F, 0);
			coltext.go();
			
			pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailQuesFour(),
					new Font(sfproMediumFont, 9.0F, 0, BASIC_DETAILS_COLOR));
			coltext.setSimpleColumn(pagecontent, 34.0F, 0.0F, 580.0F, 575.0F, 10.0F, 0);
			coltext.go();
			
			pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailQuesFive(),
					new Font(sfproMediumFont, 9.0F, 0, BASIC_DETAILS_COLOR));
			coltext.setSimpleColumn(pagecontent, 34.0F, 0.0F, 580.0F, 557.0F, 10.0F, 0);
			coltext.go();
			pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailQuesSix(),
					new Font(sfproMediumFont, 9.0F, 0, BASIC_DETAILS_COLOR));
			coltext.setSimpleColumn(pagecontent, 34.0F, 0.0F, 580.0F, 539.0F, 10.0F, 0);
			coltext.go();
			pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailQuesSeven(),
					new Font(sfproMediumFont, 9.0F, 0, BASIC_DETAILS_COLOR));
			coltext.setSimpleColumn(pagecontent, 34.0F, 0.0F, 580.0F, 521.0F, 10.0F, 0);
			coltext.go();

			pagecontent = new Phrase(basicDetailQuesAnsBean.getBasicDetailAnsFirst(),
					new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE));
			coltext.setSimpleColumn(pagecontent, 190.0F, 0.0F, 580.0F, 630.0F, 10.0F, 0);
			coltext.go();
			
			Paragraph ansThird = new Paragraph();
			ansThird.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
			ansThird.add(new Chunk(basicDetailQuesAnsBean.getBasicDetailAnsThird(),
			new Font(sfproBoldFont, 9.0F, 0, DARK_ORANGE)));
			coltext.setSimpleColumn(ansThird, 190.0F, 0.0F, 580.0F, 611.0F, 10.0F, 0);
			coltext.go();
			
			String lifeGoalHeading = "Life's BIG Goals";
			String customerName = commonService.camelCase(pdfQuestionBean.getQ27());
			String customerSaving = "You will need to save";
			String yearCount = "Every month for next";
			String assumingRate = "Assuming at:";
			String inflationPercentage = pdfQuestionBean.getQ8() + "%";
			String rateOfReturnPercentage = pdfQuestionBean.getQ9() + "%";
			String inflationRate = "INFLATION RATE";
			String rateOfReturn = "RATE OF RETURN";

			if (!protectionOfMyFamily.equalsIgnoreCase(finalPlanDetailBean.getBasicDetails())) {
				Paragraph protectionInfo = new Paragraph();
				String userMonthlyPayout = commonService
						.commaSepratedIndianFormat(finalPlanDetailBean.getMonthlyPayout());
				protectionInfo.add(new Chunk(rs, new Font(rupeesSymbol, 15.0F, 1, DARK_ORANGE)));
				protectionInfo.add(new Chunk(userMonthlyPayout, new Font(sfproMediumFont, 15.0F, 1, DARK_ORANGE)));
				Paragraph info2 = new Paragraph();
				info2.add(new Chunk(term, new Font(sfproMediumFont, 15.0F, 1, DARK_ORANGE)));
				info2.add(new Chunk("Yrs", new Font(sfproMediumFont, 15.0F, 1, DARK_ORANGE)));
				
				pagecontent = new Phrase(lifeGoalHeading, new Font(sfproMediumFont, 13.0F, 0, new Color(35, 31, 32)));
				coltext.setSimpleColumn(pagecontent, 40.0F, 0.0F, 580.0F, 450.0F, 10.0F, 0);
				coltext.go();

				pagecontent = new Phrase(customerName, new Font(sfproMediumFont, 10.0F, 0, BASIC_DETAILS_COLOR));
				coltext.setSimpleColumn(pagecontent, 40.0F, 0.0F, 580.0F, 420.0F, 10.0F, 0);
				coltext.go();
				
				pagecontent = new Phrase(customerSaving, new Font(sfproBoldFont, 10.0F, 0, BASIC_DETAILS_COLOR));
				coltext.setSimpleColumn(pagecontent, 40.0F, 0.0F, 580.0F, 405.0F, 10.0F, 0);
				coltext.go();
				
				
				pagecontent = new Phrase(planSavingAns, new Font(sfproBoldFont, 10.0F, 0, BASIC_DETAILS_COLOR));
				coltext.setSimpleColumn(pagecontent, 406.0F, 0.0F, 580.0F, 400.0F, 10.0F, 0);
				coltext.go();
				System.out.println("planTargetYrsAns "+planTargetYrsAns);
				if(planTargetYrsAns.equalsIgnoreCase("0"))
				{
					pagecontent = new Phrase(planTargetYrsAns, new Font(sfproBoldFont, 10.0F, 0, BASIC_DETAILS_COLOR));
					coltext.setSimpleColumn(pagecontent, 324.0F, 0.0F, 580.0F, 266.0F, 10.0F, 0);
					coltext.go();
					pagecontent = new Phrase("Gap", new Font(sfproBoldFont, 10.0F, 0, BASIC_DETAILS_COLOR));				
					coltext.setSimpleColumn(pagecontent, 376.0F, 0.0F, 580.0F, 309.0F, 10.0F, 0);
					coltext.go();
					Paragraph planGapAnsforGraph = new Paragraph();
					planGapAnsforGraph.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, BASIC_DETAILS_COLOR)));
					planGapAnsforGraph.add(new Chunk(planGapAns,
					new Font(sfproBoldFont, 9.0F, 0, BASIC_DETAILS_COLOR)));
					coltext.setSimpleColumn(planGapAnsforGraph, 367.0F, 0.0F, 580.0F, 296.0F, 10.0F, 0);
					coltext.go();
				}
				else
				{
					pagecontent = new Phrase(planTargetYrsAns, new Font(sfproBoldFont, 10.0F, 0, BASIC_DETAILS_COLOR));
					coltext.setSimpleColumn(pagecontent, 317.0F, 0.0F, 580.0F, 341.0F, 10.0F, 0);
					coltext.go();
					
					pagecontent = new Phrase("Gap", new Font(sfproBoldFont, 10.0F, 0, BASIC_DETAILS_COLOR));				
					coltext.setSimpleColumn(pagecontent, 369.0F, 0.0F, 580.0F, 309.0F, 10.0F, 0);
					coltext.go();
					
					Paragraph planGapAnsForGraph = new Paragraph();
					planGapAnsForGraph.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, BASIC_DETAILS_COLOR)));
					planGapAnsForGraph.add(new Chunk(planGapAns,
					new Font(sfproBoldFont, 9.0F, 0, BASIC_DETAILS_COLOR)));
					coltext.setSimpleColumn(planGapAnsForGraph, 358.0F, 0.0F, 580.0F, 296.0F, 10.0F, 0);
					coltext.go();
				}
				
				
				String currentYear=calculateTimeDiff.getCurrentYear()+"";
				pagecontent = new Phrase(currentYear, new Font(sfproBoldFont, 10.0F, 0, BASIC_DETAILS_COLOR));				
				coltext.setSimpleColumn(pagecontent, 307.0F, 0.0F, 580.0F, 240.0F, 10.0F, 0);
				coltext.go();
				pagecontent = new Phrase(getMidYearDifference(currentYear,term), new Font(sfproBoldFont, 10.0F, 0, BASIC_DETAILS_COLOR));				
				coltext.setSimpleColumn(pagecontent, 372.0F, 0.0F, 580.0F, 240.0F, 10.0F, 0);
				coltext.go();
				pagecontent = new Phrase(getLastYear(currentYear,term), new Font(sfproBoldFont, 10.0F, 0, BASIC_DETAILS_COLOR));				
				coltext.setSimpleColumn(pagecontent, 437.0F, 0.0F, 580.0F, 240.0F, 10.0F, 0);
				coltext.go();
				
				
				
				coltext.setSimpleColumn(protectionInfo, 40.0F, 0.0F, 580.0F, 380.0F, 10.0F, 0);
				coltext.go();

				pagecontent = new Phrase(yearCount, new Font(sfproBoldFont, 10.0F, 0, BASIC_DETAILS_COLOR));
				coltext.setSimpleColumn(pagecontent, 40.0F, 0.0F, 580.0F, 365.0F, 10.0F, 0);
				coltext.go();
				coltext.setSimpleColumn(info2, 40.0F, 0.0F, 580.0F, 340.0F, 10.0F, 0);
				coltext.go();

				pagecontent = new Phrase(assumingRate, new Font(sfproMediumFont, 10.0F, 0, BASIC_DETAILS_COLOR));
				coltext.setSimpleColumn(pagecontent, 40.0F, 0.0F, 580.0F, 320.0F, 10.0F, 0);
				coltext.go();

				pagecontent = new Phrase(inflationPercentage, new Font(sfproMediumFont, 10.0F, 0, WHITE_COLOR));
				coltext.setSimpleColumn(pagecontent, 52.0F, 0.0F, 580.0F, 285.0F, 10.0F, 0);
				coltext.go();

				pagecontent = new Phrase(rateOfReturnPercentage, new Font(sfproMediumFont, 10.0F, 0, WHITE_COLOR));
				coltext.setSimpleColumn(pagecontent, 52.0F, 0.0F, 580.0F, 253.0F, 10.0F, 0);
				coltext.go();

				pagecontent = new Phrase(inflationRate, new Font(sfproMediumFont, 9.0F, 0, BASIC_DETAILS_COLOR));
				coltext.setSimpleColumn(pagecontent, 110.0F, 0.0F, 580.0F, 286.0F, 10.0F, 0);
				coltext.go();

				pagecontent = new Phrase(rateOfReturn, new Font(sfproMediumFont, 9.0F, 0, BASIC_DETAILS_COLOR));
				coltext.setSimpleColumn(pagecontent, 110.0F, 0.0F, 580.0F, 256.0F, 10.0F, 0);
				coltext.go();
				genricImage = Image.getInstance(workingDir + File.separator + "genricImage.jpg");
				genricImage.scalePercent(48.0F);
				genricImage.setAbsolutePosition(0.0F, 0.0F);
				genricImage.scaleAbsoluteWidth(595.0F);
				customImage = Image.getInstance(workingDir + File.separator + "custom.jpg");
				customImage.scalePercent(41.0F);
				customImage.setAbsolutePosition(25.3F, 35.2F);
				doc.add(genricImage);
				doc.add(customImage);
				
				
					if(planTargetYrsAns.equalsIgnoreCase("0"))
					{
					imageGraph = Image.getInstance(workingDir + File.separator +"graph1.png");
					}
					else
					{
					imageGraph = Image.getInstance(workingDir + File.separator +"graph2.png");
					}
					imageGraph.scalePercent(70.0F);
					imageGraph.setAbsolutePosition(275.3F, 220.2F);
				    doc.add(imageGraph);
				    
				    
				    
				    pagecontent = new Phrase(planName, new Font(sfproBoldFont, 14.0F, 0, WHITE_COLOR));
					coltext.setSimpleColumn(pagecontent, 50.0F, 0.0F, 480.0F, 203.0F, 12.0F, 0);
					coltext.go();

					pagecontent = new Phrase(planSaving, new Font(sfproMediumFont, 10.0F, 0, BASIC_DETAILS_COLOR));
					coltext.setSimpleColumn(pagecontent, 28.0F, 0.0F, 145.0F, 170.0F, 10.0F, 0);
					coltext.go();

					pagecontent = new Phrase(planTargetYrs, new Font(sfproMediumFont, 10.0F, 0, BASIC_DETAILS_COLOR));
					coltext.setSimpleColumn(pagecontent, 30.0F, 0.0F, 110.0F, 131.0F, 10.0F, 0);
					coltext.go();

					pagecontent = new Phrase(planGap, new Font(sfproMediumFont, 10.0F, 0, BASIC_DETAILS_COLOR));
					coltext.setSimpleColumn(pagecontent, 30.0F, 0.0F, 110.0F, 96.0F, 10.0F, 0);
					coltext.go();

					pagecontent = new Phrase(planTarget, new Font(sfproMediumFont, 10.0F, 0, BASIC_DETAILS_COLOR));
					coltext.setSimpleColumn(pagecontent, 30.0F, 0.0F, 110.0F, 63.0F, 10.0F, 0);
					coltext.go();

					Paragraph a11 = new Paragraph();

					a11.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
					a11.add(new Chunk(planSavingAns, new Font(sfproBoldFont, 10.0F, 0, DARK_ORANGE)));
					coltext.setSimpleColumn(a11, 130.0F, 0.0F, 250.0F, 170.0F, 10.0F, 0);
					coltext.go();

					Paragraph a22 = new Paragraph();

					a22.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
					a22.add(new Chunk(planTargetYrsAns, new Font(sfproBoldFont, 10.0F, 0, DARK_ORANGE)));
					coltext.setSimpleColumn(a22, 130.0F, 0.0F, 250.0F, 131.0F, 10.0F, 0);
					coltext.go();

					Paragraph a33 = new Paragraph();

					a33.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
					a33.add(new Chunk(planGapAns, new Font(sfproBoldFont, 10.0F, 0, DARK_ORANGE)));
					coltext.setSimpleColumn(a33, 130.0F, 0.0F, 250.0F, 96.0F, 10.0F, 0);
					coltext.go();

					Paragraph a44 = new Paragraph();

					a44.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
					a44.add(new Chunk(planTargetAns, new Font(sfproBoldFont, 10.0F, 0, DARK_ORANGE)));
					coltext.setSimpleColumn(a44, 130.0F, 0.0F, 250.0F, 63.0F, 10.0F, 0);
					coltext.go();
				    
				    
				   
			} else {
				
				pagecontent = new Phrase(lifeGoalHeading, new Font(sfproMediumFont, 13.0F, 0, new Color(35, 31, 32)));
				coltext.setSimpleColumn(pagecontent, 40.0F, 0.0F, 580.0F, 475.0F, 10.0F, 0);
				coltext.go();
				
				protectionImage = Image.getInstance(workingDir + File.separator + "protectionImage.jpg");
				protectionImage.scalePercent(48.0F);
				protectionImage.setAbsolutePosition(0.0F, 234.0F);
				protectionImage.scaleAbsoluteWidth(595.0F);
			
				customProtectionImage = Image.getInstance(workingDir + File.separator + "custom.jpg");
				customProtectionImage.scalePercent(41.0F);
				customProtectionImage.setAbsolutePosition(25.3F, 280.2F);
				
				doc.add(protectionImage);
				doc.add(customProtectionImage);

				
				
				pagecontent = new Phrase(planName, new Font(sfproBoldFont, 14.0F, 0, WHITE_COLOR));
				coltext.setSimpleColumn(pagecontent, 45.0F, 0.0F, 250.0F, 448.0F, 12.0F, 0);
				coltext.go();

				pagecontent = new Phrase(planSaving, new Font(sfproMediumFont, 10.0F, 0, BASIC_DETAILS_COLOR));
				coltext.setSimpleColumn(pagecontent, 28.0F, 0.0F, 145.0F, 425.0F, 10.0F, 0);
				coltext.go();

				pagecontent = new Phrase(planTargetYrs, new Font(sfproMediumFont, 10.0F, 0, BASIC_DETAILS_COLOR));
				coltext.setSimpleColumn(pagecontent, 30.0F, 0.0F, 110.0F, 386.0F, 10.0F, 0);
				coltext.go();

				pagecontent = new Phrase(planGap, new Font(sfproMediumFont, 10.0F, 0, BASIC_DETAILS_COLOR));
				coltext.setSimpleColumn(pagecontent, 30.0F, 0.0F, 110.0F, 346.0F, 10.0F, 0);
				coltext.go();

				pagecontent = new Phrase(planTarget, new Font(sfproMediumFont, 10.0F, 0, BASIC_DETAILS_COLOR));
				coltext.setSimpleColumn(pagecontent, 30.0F, 0.0F, 110.0F, 313.0F, 10.0F, 0);
				coltext.go();

				Paragraph a11 = new Paragraph();

				a11.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
				a11.add(new Chunk(planSavingAns, new Font(sfproBoldFont, 10.0F, 0, DARK_ORANGE)));
				coltext.setSimpleColumn(a11, 135.0F, 0.0F, 250.0F, 425.0F, 10.0F, 0);
				coltext.go();

				Paragraph a22 = new Paragraph();

				a22.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
				a22.add(new Chunk(planTargetYrsAns, new Font(sfproBoldFont, 10.0F, 0, DARK_ORANGE)));
				coltext.setSimpleColumn(a22, 130.0F, 0.0F, 250.0F, 386.0F, 10.0F, 0);
				coltext.go();

				Paragraph a33 = new Paragraph();

				a33.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
				a33.add(new Chunk(planGapAns, new Font(sfproBoldFont, 10.0F, 0, DARK_ORANGE)));
				coltext.setSimpleColumn(a33, 130.0F, 0.0F, 250.0F, 346.0F, 10.0F, 0);
				coltext.go();

				Paragraph a44 = new Paragraph();

				a44.add(new Chunk(rs, new Font(rupeesSymbol, 10.0F, 1, DARK_ORANGE)));
				a44.add(new Chunk(planTargetAns, new Font(sfproBoldFont, 10.0F, 0, DARK_ORANGE)));
				coltext.setSimpleColumn(a44, 130.0F, 0.0F, 250.0F, 313.0F, 10.0F, 0);
				coltext.go();
				
				
				
				
				
				
			}
			
			dyanamicImage = getDyanamicImage(workingDir, planNameForImage);
			
			
			

			
			
			
			doc.newPage();
			doc.add(backgroundImage);

			
			pagecontent = new Phrase("Recommended products for you",
					new Font(sfproMediumFont, 12.0F, 0, new Color(35, 31, 32)));
			coltext.setSimpleColumn(pagecontent, 25.0F, 0.0F, 580.0F, 825.0F, 10.0F, 0);
			coltext.go();
			doc.add(dyanamicImage);

		} catch (Exception ex) {
			logger.info("creating exception while making pdf and creating pdf " + ex);
		} finally {
			try {
				if (doc != null) {
					doc.close();
				}

				if (retrunOutput != null) {
					retrunOutput.close();
				}
			} catch (Exception ex) {
				
				logger.info("creating exception while closing conn " + ex);
			}

		}
		logger.info("End executing method TextPdfcreator ");
		return retrunOutput;

	}
    private Image getDyanamicImage(String workingDir, String planNameForImage) 
    {
    	
    	Image dyanamicImage=null;
    	try {
    	dyanamicImage = Image.getInstance(workingDir + File.separator + planNameForImage.split(" ")[1].trim().toUpperCase() + ".png");
		dyanamicImage.scalePercent(40.0F);
		dyanamicImage.setAbsolutePosition(25.0F, 420.0F);
    } catch (Exception e) {
		logger.info("creating exception while getting dynamic image  in getDyanamicImage" + e);
	}
		return dyanamicImage;
	}



	private String getLastYear(String currentYear, String term) {
		String lastYear="";
    	
		try
    	{
			lastYear=Integer.parseInt(currentYear)+Integer.parseInt(term)+"";
    	}catch(Exception e)
    	{
    		logger.error("creating exception in getLastYear "+e);
    	}
		return lastYear;
	}



	private String getMidYearDifference(String currentYear, String term) {
		String midYear="";
		int average=0;
    	try 
    	{
    		average=Integer.parseInt(term);
    		average=(average/2);
    		midYear=average+Integer.parseInt(currentYear)+"";
    		
    	}catch(Exception e)
    	{
    		
    		logger.error("Creating exception in getMidYearDifference :: PDF Creation Service Impl .."+e);
    	}
		return midYear;
	}



	@Override
	public FinalPlanDetailBean finalPlanDetail(Map<String, String> plan)
	{

		for (Entry<String, String> entry : plan.entrySet())
		{
			String key = entry.getKey();
			String value = entry.getValue();

			if ("ChildMarriage".equalsIgnoreCase(key))
			{

				finalPlanDetailBean=setPlanData(childMarriage,value);
				
				
			}
			if ("protectionofmyfamily".equalsIgnoreCase(key))
			{
				
				
				finalPlanDetailBean=setPlanData(protectionOfMyFamily,value);
				

			} else if ("RegularSaving".equalsIgnoreCase(key))
			{
				finalPlanDetailBean=setPlanData("Regular Saving",value);
			} else if (retirement.equalsIgnoreCase(key))
			{
				finalPlanDetailBean=setPlanData(retirement,value);
			} else if ("ChildEducation".equalsIgnoreCase(key))
			{
				finalPlanDetailBean=setPlanData("Child Education",value);
				
			} else if ("ChildMarriage".equalsIgnoreCase(key)) 
			{
				finalPlanDetailBean=setPlanData(childMarriage,value);
			}
		}
		return finalPlanDetailBean;
	}
	
	private FinalPlanDetailBean setPlanData(String planName, String value)
	{
		finalPlanDetailBean.setBasicDetails(planName);
		finalPlanDetailBean.setGap(value.split(",")[0].split("=")[1]);
		finalPlanDetailBean.setMonthlyPayout(value.split(",")[3].split("=")[1]);
		finalPlanDetailBean.setSavingTillDate(value.split(",")[2].split("=")[1]);
		finalPlanDetailBean.setTotalSaving(value.split(",")[1].split("=")[1]);
		
		return finalPlanDetailBean;
	}



	

	private PDFQuestionBean getQuestions(Map detailsMap, String sessionId) {

		for (Entry<String, String> entry : ((Map<String, String>) ((Map) detailsMap.get(sessionId + "QuestionAswerMap"))
				.get(sessionId)).entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			if ("Q13".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ13(value);
			} else if ("Q25".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ25(value);
			} else if ("Q14".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ14(value);
			} else if ("Q15".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ15(value);
			} else if ("Q16".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ16(value);
			} else if ("Q17".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ17(value);
			} else if ("Q18".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ18(value);
			} else if ("Q19".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ19(value);
			} else if ("Q20".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ20(value);
			} else if ("Q21".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ21(value);
			} else if ("Q22".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ22(value);
			} else if ("Q23".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ23(value);
			} else if ("Q24".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ24(value);
			} else if ("Q1".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ1(value);
			} else if ("Q7".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ7(value);
			} else if ("Q3".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ3(value);
			} else if ("Q4".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ4(value);
			} else if ("Q5".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ5(value);
			} else if ("Q2".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ2(value);
			} else if ("Q26".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ26(value);
			} else if ("Q10".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ10(value);
			} else if ("Q11".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ11(value);
			} else if ("Q12".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ12(value);
			} else if ("Q27".equalsIgnoreCase(key)) {
				pDFQuestionBean.setQ27(value);
			} else if ("Q8".equalsIgnoreCase(key)) {
				if ("N".equalsIgnoreCase(value)) {
					pDFQuestionBean.setQ8("8");
				} else {
					pDFQuestionBean.setQ8(value);
				}
			} else if ("Q9".equalsIgnoreCase(key)) {
				if ("N".equalsIgnoreCase(value)) {
					pDFQuestionBean.setQ9("8");
				} else {
					pDFQuestionBean.setQ9(value);
				}
			}
		}
		return pDFQuestionBean;
	}
}
