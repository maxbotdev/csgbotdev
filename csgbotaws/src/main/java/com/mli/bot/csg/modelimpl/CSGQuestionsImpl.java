package com.mli.bot.csg.modelimpl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.model.CSGQuestions;
import com.mli.bot.csg.service.CSGQuestionsCall;

@Service
public class CSGQuestionsImpl implements CSGQuestions
{
	@Autowired
	private CSGQuestionsCall csgQuestionCall;
	
	private static Logger logger = LogManager.getLogger(CSGQuestionsImpl.class);
	
	String speech="";
	@Override
	public String csgQuestions(Map<String,Map<String,String>> externalMap, String sessionId, String action) 
	{
	try {
		Map<String,String> internalMap = new LinkedHashMap();
		String questionResponse=csgQuestionCall.csgQuestionsServiceCall(externalMap, sessionId, action);
		JSONArray jsonArr = new JSONArray(questionResponse);
		for(int i=0; i<jsonArr.length();i++)
		{
			String result = jsonArr.get(i)+"";
			String [] arr  = result.split(",");
			internalMap.put(arr[0], arr[1]);
		}
		externalMap.put(sessionId, internalMap);
		speech=externalMap.get(sessionId+"Msg").get("CSGQuestions");
	}catch(Exception e) {
		logger.error("creating exception in "+e);
	}
		return speech;
	}
}
