package com.mli.bot.csg.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mli.bot.csg.commons.BeanProperty;
import com.mli.bot.csg.service.ChildMarriageCalculator;

@Service
public class ChildMarriageCalculatorImpl implements ChildMarriageCalculator 
{
	@Autowired
	private BeanProperty bean;
	
	String resultChildMarriage;
	@Override
	public String childMarriageCalculator(String age, String marriageAge, String neededMoney, String asideMoney, 
			String expectSaving,String inflationRate) 
	{
		String url=bean.getGetChildMarriagePremium();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"age\":\""+age+"\",	");
		sb.append("	  \"marriageAge\":\""+marriageAge+"\",	");
		sb.append("	  \"neededMoney\":\""+neededMoney+"\",	");
		sb.append("	  \"asideMoney\":\""+asideMoney+"\",	");
		sb.append("	  \"expectSaving\":\""+expectSaving+"\",	");
		sb.append("	  \"inflationRate\":\""+inflationRate+"\"	");
		sb.append("	}	");
		HttpEntity<String> entity=new HttpEntity<>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			resultChildMarriage = response.getBody();
		}
		return resultChildMarriage;
	}
	
	
	
}
