package com.mli.bot.csg.service;

public interface ProtectionofmyfamilyCalc 
{
	public String protectionOfMyFamilyCalculation(String age, String monthlyExp, String savingInvest, String loans, String futureGoal, String existingCover);

}
