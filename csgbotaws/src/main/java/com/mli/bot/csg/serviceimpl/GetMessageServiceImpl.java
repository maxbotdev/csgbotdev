package com.mli.bot.csg.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mli.bot.csg.commons.BeanProperty;
import com.mli.bot.csg.service.GetMessageService;



@Service
public class GetMessageServiceImpl implements GetMessageService {
	
	@Autowired
	private BeanProperty bean;

	String textResponse;
	@Override
	public String getCsgMessages(String botName){
		String url = bean.getGetMessageAPI();
		ResponseEntity<String> response=null;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"type\":\""+botName+"\"	");
		sb.append("	}	");

		HttpEntity<String> entity=new HttpEntity<>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		
		response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			textResponse = response.getBody();

		}
		return textResponse;
	}

}
