package com.mli.bot.csg.service;

public interface GetUlipDataService {

	public String getUlipData(String age,String term,String pocket,String plan);
	
}
