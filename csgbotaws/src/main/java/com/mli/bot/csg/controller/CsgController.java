package com.mli.bot.csg.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mli.bot.csg.exceptions.GenericCustomException;
import com.mli.bot.csg.handlerinterface.HandlerFactoryPattern;
import com.mli.bot.csg.handlerinterface.RequestResponseHandler;
import com.mli.bot.csg.request.WebhookRequest;
import com.mli.bot.csg.response.GenericResponse;
import com.mli.bot.csg.service.NeedAnalysisService;


@RestController
public class CsgController {

	private static Logger logger = LogManager.getLogger(CsgController.class);
	
	@Autowired
	private NeedAnalysisService needAnalysisService;
	
	@Autowired
	private HandlerFactoryPattern factoryPattern;
	
	/**
	 * Schedular is configured to run on every 10Min and remove all un-used session's from cache
	 * i.e. those sessions which are ideal from past 10 Min.
	 */
	
	@Scheduled(cron = "0 0/10 * * * ?")
	public void removeCashethirtyminute()
	{
		logger.info("Cron job to remove un-used session from cache : Start");
		needAnalysisService.removeUnUsedSessionFromCache();
	}
	
	
	/**
	 * @param request : Request data in JSON Format
	 * @return
	 * @throws GenericCustomException 
	 */
	@RequestMapping(path ="/csg")
	public GenericResponse webhook(@RequestBody WebhookRequest webReq) 
	{
		logger.info("csg bot process starts");
		RequestResponseHandler  reqRpnsHandler = factoryPattern.getHandlerObject(webReq);
		logger.info("csg bot process Ends");
		return  reqRpnsHandler.getResponse();
	}
	
}
