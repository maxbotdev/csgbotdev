package com.mli.bot.csg.beans;

import org.springframework.stereotype.Component;

@Component
public class FinalPlanDetailBean
{
	private String basicDetails;
	private String totalSaving;
	private String savingTillDate;
	private String gap;
	private String monthlyPayout;
	public String getBasicDetails() {
		return basicDetails;
	}
	public void setBasicDetails(String basicDetails) {
		this.basicDetails = basicDetails;
	}
	public String getTotalSaving() {
		return totalSaving;
	}
	public void setTotalSaving(String totalSaving) {
		this.totalSaving = totalSaving;
	}
	public String getSavingTillDate() {
		return savingTillDate;
	}
	public void setSavingTillDate(String savingTillDate) {
		this.savingTillDate = savingTillDate;
	}
	public String getGap() {
		return gap;
	}
	public void setGap(String gap) {
		this.gap = gap;
	}
	public String getMonthlyPayout() {
		return monthlyPayout;
	}
	public void setMonthlyPayout(String monthlyPayout) {
		this.monthlyPayout = monthlyPayout;
	}
	@Override
	public String toString() {
		return "FinalPlanDetailBean [basicDetails=" + basicDetails + ", totalSaving=" + totalSaving
				+ ", savingTillDate=" + savingTillDate + ", gap=" + gap + ", monthlyPayout=" + monthlyPayout + "]";
	}
}
