package com.mli.bot.csg.serviceimpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mli.bot.csg.commons.BeanProperty;
import com.mli.bot.csg.service.ProtectionofmyfamilyCalc;

@Service
public class ProtectionofmyfamilyCalcImpl implements ProtectionofmyfamilyCalc
{
	@Autowired
	private BeanProperty bean;
	String resultProtectToMyFamily="";
	@Override
	public String protectionOfMyFamilyCalculation(String age, String monthlyExp, String savingInvest, String loans, String futureGoal, String existingCover)
	{
		
		String url=bean.getGetProtectionPremium();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"age\":\""+age+"\",	");
		sb.append("	  \"monthlyExp\":\""+monthlyExp+"\",	");
		sb.append("	  \"savingInvest\":\""+savingInvest+"\",	");
		sb.append("	  \"loans\":\""+loans+"\",	");
		sb.append("	  \"futureGoal\":\""+futureGoal+"\",	");
		sb.append("	  \"existingCover\":\""+existingCover+"\"	");
		sb.append("	}	");
		HttpEntity<String> entity=new HttpEntity<>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			resultProtectToMyFamily = response.getBody();
		}
		return resultProtectToMyFamily;
	}
}