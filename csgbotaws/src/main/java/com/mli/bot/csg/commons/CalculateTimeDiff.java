package com.mli.bot.csg.commons;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class CalculateTimeDiff 
{
	private static Logger logger = LogManager.getLogger(CalculateTimeDiff.class);
	
	/**
	 * @param currentTime
	 * @param loginTime
	 * @return
	 */
	private String dateFormat="MM/dd/yyyy HH:mm:ss";
	/**
	 * @param currentTime
	 * @param loginTime
	 * @return
	 */
	public long timeDiffInMinute(String currentTime, String loginTime)
	{
		long diffInMinutes=0;
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		Date d1 = null;
		Date d2 = null;
		try 
		{
			d1 = format.parse(loginTime);
			d2 = format.parse(currentTime);
			long diffInMilliseconds = d2.getTime() - d1.getTime();
			diffInMinutes = diffInMilliseconds / (60 * 1000) % 60;
		} 
		catch (Exception e) 
		{
			logger.error("We are in exception while calculating difference in minute - dates : "+currentTime+" : "+loginTime+" :: "+e);
		}
		return diffInMinutes;
	}
	
	/**
	 * @param currentTime
	 * @param loginTime
	 * @return
	 */
	public long timeDiffInSeconds(String currentTime, String loginTime)
	{
		long diffInSeconds=0;
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		Date d1 = null;
		Date d2 = null;
		try 
		{
			d1 = format.parse(loginTime);
			d2 = format.parse(currentTime);
			long diffInMilliseconds = d2.getTime() - d1.getTime();
			diffInSeconds = diffInMilliseconds / 1000 % 60;
		} 
		catch (Exception e) 
		{
			logger.error("We are in exception while calculating difference in Seconds - dates : "+currentTime+" : "+loginTime+" :: "+e);
		}
		return diffInSeconds;
	}
	
	/**
	 * @param currentTime
	 * @param loginTime
	 * @return
	 */
	public long timeDiffInHours(String currentTime, String loginTime)
	{
		long diffInHours=0;
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		Date d1 = null;
		Date d2 = null;
		try 
		{
			d1 = format.parse(loginTime);
			d2 = format.parse(currentTime);
			long diffInMilliseconds = d2.getTime() - d1.getTime();
			diffInHours = diffInMilliseconds / (60 * 60 * 1000) % 24;
		} 
		catch (Exception e) 
		{
			logger.error("We are in exception while calculating difference in Hours - dates : "+currentTime+" : "+loginTime+" :: "+e);
		}
		return diffInHours;
	}
	
	/**
	 * @param currentTime
	 * @param loginTime
	 * @return
	 */
	public long timeDiffInDays(String currentTime, String loginTime)
	{
		long diffInDays=0;
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		Date d1 = null;
		Date d2 = null;
		try 
		{
			d1 = format.parse(loginTime);
			d2 = format.parse(currentTime);
			long diffInMilliseconds = d2.getTime() - d1.getTime();
			diffInDays = diffInMilliseconds / (24 * 60 * 60 * 1000);
		} 
		catch (Exception e) 
		{
			logger.error("We are in exception while calculating difference in Days - dates : "+currentTime+" : "+loginTime+" :: "+e);
		}
		return diffInDays;
	}
	
	public int getCurrentYear() {
		return  Calendar.getInstance().get(Calendar.YEAR);
	}
	
	
	
	
}
