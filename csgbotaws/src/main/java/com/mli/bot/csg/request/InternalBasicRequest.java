package com.mli.bot.csg.request;

public class InternalBasicRequest {

	private String session;
	private String queryText;
	private String action;
	private Parameters parameters;

	public InternalBasicRequest() {
		parameters=new Parameters();
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getQueryText() {
		return queryText;
	}

	public void setQueryText(String queryText) {
		this.queryText = queryText;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Parameters getParameters() {
		return parameters;
	}

	public void setParameters(Parameters parameters) {
		this.parameters = parameters;
	}

}
