package com.mli.bot.csg.response;

public class ContextData 
{
	private String name;
	private String lifespan;
	private String policyNumberOriginal;
	private InnerContextData parameters;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLifespan() {
		return lifespan;
	}
	public void setLifespan(String lifespan) {
		this.lifespan = lifespan;
	}
	public String getPolicyNumberOriginal() {
		return policyNumberOriginal;
	}
	public void setPolicyNumberOriginal(String policyNumberOriginal) {
		this.policyNumberOriginal = policyNumberOriginal;
	}
	public InnerContextData getParameters() {
		return parameters;
	}
	public void setParameters(InnerContextData parameters) {
		this.parameters = parameters;
	}
	
}
