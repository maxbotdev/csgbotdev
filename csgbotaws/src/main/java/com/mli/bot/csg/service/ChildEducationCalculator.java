package com.mli.bot.csg.service;

public interface ChildEducationCalculator 
{
	public String childEducationCalculator(String age, String graduation, String studyType, String country, String currentSavings, 
			String childAge,String inflationRate, String rateReturnInvestment);

}
