package com.mli.bot.csg.calculatorimpl;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.calculator.RegularSavings;
import com.mli.bot.csg.service.RegularSavingsCalc;

@Service
public class RegularSavingsImpl implements RegularSavings {
	@Autowired
	private RegularSavingsCalc regularSavingsCalc;

	@Override
	public String regularSavings(Map<String, Map<String, String>> questionAswerMap, String sessionId) {
		String age = "";
		String goalAge = "";
		String currentCostOfGoal = "";
		String currentProvision = "";
		String inflationRate = "";
		String rateReturnInvestment = "";
		for (Entry<String, String> entry : questionAswerMap.get(sessionId).entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			if ("Q13".equalsIgnoreCase(key)) {
				age = value;
			} else if ("Q21".equalsIgnoreCase(key)) 
			{
				goalAge = value;
			} else if ("Q22".equalsIgnoreCase(key)) 
			{
				currentCostOfGoal = value;
			} else if ("Q23".equalsIgnoreCase(key)) 
			{
				currentProvision = value;
			} else if ("Q8".equalsIgnoreCase(key)) 
			{
				inflationRate = value;
				if ("N".equalsIgnoreCase(inflationRate)) {
					inflationRate = "8";
				}
			} else if ("Q9".equalsIgnoreCase(key)) 
			{
				rateReturnInvestment = value;
				if ("N".equalsIgnoreCase(rateReturnInvestment)) {
					rateReturnInvestment = "8";
				}
			}
		}
		return regularSavingsCalc.regularSavingCalculation(age, goalAge, currentCostOfGoal,
				currentProvision, inflationRate, rateReturnInvestment);
	}

}
