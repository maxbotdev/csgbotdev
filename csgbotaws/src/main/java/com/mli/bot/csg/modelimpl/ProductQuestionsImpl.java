package com.mli.bot.csg.modelimpl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.model.ProductQuestions;
import com.mli.bot.csg.service.ProductDetails;
@Service
public class ProductQuestionsImpl implements ProductQuestions{
	private static Logger logger = LogManager.getLogger(ProductQuestionsImpl.class);	
	@Autowired
	private ProductDetails productDetails;
	String speech="";
	
	@Override
	public String productQuestions(Map<String, Map<String, String>> externalMap, String sessionId, String product) {
		String[] str=product.split(" ");
		logger.info("start method productQuestions ..");
		Map<String, String> productMap =new HashMap();
		productMap.put("product",str[1]);
		
		externalMap.put(sessionId+"productName",productMap);
		Map<String,String> internalMap = new LinkedHashMap<>();
		String questionResponse=productDetails.productdetailsQuestion(externalMap, sessionId, str[1]);
		questionResponse=questionResponse.replaceAll("\"", "");
		internalMap.put("ProductDetailsQuestion", questionResponse);
		externalMap.put(sessionId+"ProductMsg", internalMap);
		speech=externalMap.get(sessionId+"ProductMsg").get("ProductDetailsQuestion");
		logger.info("End method productQuestions .."+speech);
		
		
		
		return speech;
	}

}
