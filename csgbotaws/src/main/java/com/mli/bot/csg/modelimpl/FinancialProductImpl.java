package com.mli.bot.csg.modelimpl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.model.FinancialProduct;
import com.mli.bot.csg.service.FinancialProductCall;

@Service
public class FinancialProductImpl implements FinancialProduct 
{
	@Autowired
	private FinancialProductCall financialProductCall;
	
	@Override
	public String financialProduct(Map<String, Map<String, String>> externalMap, String sessionId, String action, String questions) {
		Map<String,String> map = new HashMap<>();
		
		String goalResult = financialProductCall.financialProductCall(externalMap, sessionId, action, questions);
		map.put("Goals", goalResult);
		externalMap.put(sessionId, map);
		
		goalResult=externalMap.get(sessionId+"Msg").get("FinancialProduct");
		return goalResult;
	}

}
