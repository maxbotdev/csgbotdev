package com.mli.bot.csg.commons;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.utils.HttpCaller;

@Service
public class Adoptionlogs 
{
	private static Logger logger = LogManager.getLogger(Adoptionlogs.class);
	@Autowired
	private BeanProperty bean;
	@Autowired
	HttpCaller httpCaller;
	public String adoptionlogsCall(String sessionId, String sSOId, String action, String resolvedQuery, String speech)
	{
		String speechmodified = "";

		speechmodified=speech;
			
		String platform="CSGBOTPROD";
		
		
		HttpURLConnection conn = null;
		StringBuilder result = new StringBuilder();
		Calendar cal = Calendar.getInstance(); // creates calendar
		cal.setTime(new Date()); // sets calendar time/date
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		try
		{
			String extURL = bean.getAdoptionlogs();
			URL url = new URL(extURL);
			if(bean.getDevMode()!=null && !"".equalsIgnoreCase(bean.getDevMode()) && "Y".equalsIgnoreCase(bean.getDevMode()))
			{
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("cachecluster.maxlifeinsurance.com", 3128));
				conn = (HttpURLConnection) url.openConnection(proxy);
			}else{
				conn = (HttpURLConnection) url.openConnection();
			}
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			StringBuilder requestdata=new StringBuilder();
			requestdata.append("	{				");
			requestdata.append("		\"header\": {		");
			requestdata.append("		\"uniqueId\": \"\",		");
			requestdata.append("		\"creationTime\": \"\",	");
			requestdata.append("		\"userId\": \"\",	");
			requestdata.append("		\"password\": \"\"	");
			requestdata.append("	},				");
			requestdata.append("		\"payload\": {			");
			requestdata.append("	     \"transaction\": [{	");
			requestdata.append("			\"session_Id\": \""+sessionId+"\",");
			requestdata.append("			\"ssoId\": \""+sSOId+"\",");
			requestdata.append("			\"kpiAsked\": \""+action+"\",		");
			requestdata.append("			\"intentCalled\": \""+resolvedQuery+"\",");
			requestdata.append("			\"platform\": \""+platform+"\",		");
			requestdata.append("			\"loginTime\": \""+dateFormat.format(cal.getTime())+"\",");
			requestdata.append("			\"apiResponse\": \""+speechmodified+"\" ");
			requestdata.append("		     }]			");
			requestdata.append("		}	");
			requestdata.append("	}	");
			httpCaller.callHttp(bean.getAdoptionlogs(), requestdata, result);
		} catch (Exception e)
		{
			logger.info("Exception in adoption log api :: " + e);
		}
		return result.toString();
	}

}