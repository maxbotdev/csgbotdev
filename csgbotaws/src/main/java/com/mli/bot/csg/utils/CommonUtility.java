package com.mli.bot.csg.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
@Component
public class CommonUtility {
	private static Logger logger = LogManager.getLogger(CommonUtility.class);	
	public String getCurrentDateAndTime() {
	Date date = new Date();
	String pattern = "MM/dd/yyyy HH:mm:ss";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	return  simpleDateFormat.format(date);
}
	
	public String nullThenBlank(String str) {
		
		if(str!=null && !"".equalsIgnoreCase(str)) {
			
			return str.trim();
		}
		return "";
	}
	
	
	public StringBuilder getButtonsResponse(String speech) 
	{
		
		StringBuilder sb = new StringBuilder();
		try {
		sb.append("	{	");
		sb.append("	  \"fulfillmentText\": \"displayed&spoken response\",	");
		sb.append("	  \"fulfillmentMessages\": [	");
		sb.append("	    {	");
		sb.append("	      \"text\": {	");
		sb.append("	        \"text\": [\""+speech+"\"]	");
		sb.append("	      }	");
		sb.append("	       }	");
		sb.append("	  ],	");
		sb.append("	  \"payload\": {	");
		sb.append("	    \"facebook\": {	");
		sb.append("	      \"type\": \"Chatbot\",	");
		sb.append("	      \"platform\": \"API.AI\",	");
		sb.append("	      \"title\": \"MLIChatBot\",	");
		sb.append("	      \"imageUrl\": \"BOT\",	");
		sb.append("	      \"buttons\": [	");
		}catch(Exception e) {
			
			logger.info("creating exception in getButtonsResponse "+e);
		}
		return sb;
	}
	
	public StringBuilder addFinalRespone() {
	StringBuilder sb3 = new StringBuilder();
	
	sb3.append("	      ]	");
	sb3.append("	    }	");
	sb3.append("	  },	");
	sb3.append("	  \"source\": \"java-webhook\"	");
	sb3.append("}	");
    
	return sb3;
	}
	
	
	public StringBuilder responseWithoutButton(String speech) {
		StringBuilder sb = new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"fulfillmentText\": \"displayed&spoken response\",	");
		sb.append("	  \"fulfillmentMessages\": [	");
		sb.append("	    {	");
		sb.append("	      \"text\": {	");
		sb.append("	        \"text\": [\""+speech+"\"]	");
		sb.append("	      }	");
		sb.append("	 }	");
		sb.append("	  ]	");
		sb.append("	 }	");
		return sb;
	}
	
	
	public StringBuilder makeDyanamicButton(String text,String postback) {
		StringBuilder sb = new StringBuilder();
		sb.append("	        {	");
		sb.append("	          \"text\": \" "+text+"\",	");
		sb.append("	          \"postback\": \""+postback+"\",	");
		sb.append("	          \"link\": \"\"	");
		sb.append("	        },	");
		return sb;
	}
	
	public StringBuilder removeLastCharcter(StringBuilder sb) {
	
		return sb.deleteCharAt(sb.length() - 2);
	}
	
	public String getParNonParAndUlipRequest(String url,String age,String term,String pocket,String plan)
	{
		String textResponse = null;
        ResponseEntity<String> response=null;
        logger.info("Start .. getParNonParAndUlipRequest ");
        try
		{
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"age\":\""+age+"\",	");
		sb.append("	  \"term\":\""+term+"\",	");
		sb.append("	  \"pocket\":\""+pocket+"\",	");
		sb.append("	  \"plan\":\""+plan+"\"	");
		sb.append("	}	");

		HttpEntity<String> entity=new HttpEntity<>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		
		response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			textResponse = response.getBody();
		}
		}catch(Exception e){
			textResponse="";
			logger.info("creating exception getParNonParAndUlipRequest "+e);
		}
		logger.info("End .. getParNonParAndUlipRequest with Response "+textResponse);
		
       return textResponse;

	}
	
	
	
	
	
	
	
}
