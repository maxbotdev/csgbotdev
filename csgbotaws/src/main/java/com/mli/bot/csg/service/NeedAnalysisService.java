package com.mli.bot.csg.service;

import com.mli.bot.csg.exceptions.GenericCustomException;
import com.mli.bot.csg.request.InternalBasicRequest;

public interface NeedAnalysisService
{
	 public void removeUnUsedSessionFromCache();
	 public String doProcessRequest(InternalBasicRequest requestObject) throws GenericCustomException;
	 public String getArciseToken();
	 public String callArciseServices(String sessionId,String sourceName);
	 
}
