package com.mli.bot.csg.model;

import java.util.Map;

public interface FinancialProduct {

	public String financialProduct(Map<String,Map<String,String>> externalMap, String sessionId, String action, String questions);
}
