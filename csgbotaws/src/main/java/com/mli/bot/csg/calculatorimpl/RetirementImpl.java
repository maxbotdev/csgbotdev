package com.mli.bot.csg.calculatorimpl;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.calculator.Retirement;
import com.mli.bot.csg.service.RetirementCalc;

@Service
public class RetirementImpl implements Retirement
{
	String responseRetirement="";
	@Autowired
	private RetirementCalc retirementCalc;
	@Override
	public String retirement(Map<String, Map<String, String>> questionAswerMap, String sessionId) 
	{
		String age="";
		String retirePlan="";
		String monthlyExp="";
		String  expAfterRetire="";
		String  inflationRate="";
		String  saving="";
		String  expectedRetSaving="";
		for(Entry<String,String> entry : questionAswerMap.get(sessionId).entrySet())
		{
			String key =entry.getKey();
			String value =entry.getValue();
			if("Q13".equalsIgnoreCase(key))
			{
				age=value;
			}
			else if("Q19".equalsIgnoreCase(key))
			{
				retirePlan=value;
			}
			else if("Q15".equalsIgnoreCase(key))
			{
				monthlyExp=value;
			}
			else if("Q24".equalsIgnoreCase(key))
			{
				expAfterRetire=value;
			}
			else if("Q8".equalsIgnoreCase(key))
			{
				inflationRate=value;
				if("N".equalsIgnoreCase(inflationRate))
				{
					inflationRate="8";
				}
			}
			else if("Q20".equalsIgnoreCase(key))
			{
				saving=value;
			}
			else if("Q9".equalsIgnoreCase(key))
			{
				expectedRetSaving=value;
				if("N".equalsIgnoreCase(expectedRetSaving))
				{
					expectedRetSaving="8";
				}
			}
		}
		return retirementCalc.retirementCalc(age, retirePlan, monthlyExp, expAfterRetire, inflationRate, saving, expectedRetSaving);
	}

}