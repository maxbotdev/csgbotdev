package com.mli.bot.csg.serviceimpl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.commons.BeanProperty;
import com.mli.bot.csg.service.GetUlipDataService;
import com.mli.bot.csg.utils.CommonUtility;

@Service
public class GetUlipDataImpl implements GetUlipDataService {

	private static Logger logger = LogManager.getLogger(GetUlipDataImpl.class);
	@Autowired
	private BeanProperty bean;
	String textResponse;
	
	
	@Autowired
	private CommonUtility commonUtility;
	
	@Override
	public String getUlipData(String age, String term, String pocket, String plan)
	{
		logger.info("Start .. GetUlipDataImpl::getUlipData ");
		try
		{
		String url=bean.getGetUlipData();
		textResponse = commonUtility.getParNonParAndUlipRequest(url, age, term, pocket, plan);
		}catch(Exception e) {
			logger.info("creating exception in  GetUlipDataImpl "+e);
		}
		logger.info("End .. GetUlipDataImpl::getUlipData ");
		
		return textResponse;
	}
	
}
