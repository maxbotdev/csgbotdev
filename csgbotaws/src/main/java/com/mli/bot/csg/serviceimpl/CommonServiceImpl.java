package com.mli.bot.csg.serviceimpl;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.commons.BeanProperty;
import com.mli.bot.csg.service.CommonService;
import com.mli.bot.csg.utils.HttpCaller;


@Service
public class CommonServiceImpl implements CommonService{
	private static Logger logger = LogManager.getLogger(CommonServiceImpl.class);
	@Autowired
	private BeanProperty bean;
	@Autowired
	HttpCaller httpCaller;
	@Override
	public byte[] convertPdfToByteArray(String source) {
		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		InputStream inputStream =null;
		try {
		 inputStream = new FileInputStream(source);
		

		int data;
		while( (data = inputStream.read()) >= 0 ) {
		    outputStream.write(data);
		}
		
		
	
	}catch(Exception e) 
	{
		logger.info("Exception in converting pdf to byte array :: "+e);
		
	}
		finally
		{
			if(inputStream!=null) {
				try {
			  inputStream.close();
				}catch(Exception e)
				{
				 logger.info("creating exception in closing inputstreem obj "+e);
				}
				
			}
		}
		return outputStream.toByteArray();
	}

	@Override
	public String sendMail(String byteArray,String mailTo,String username) {
		byteArray=byteArray.replaceAll("\n","");
		StringBuilder result = new StringBuilder();
		Random rand = new Random(); 
		int correlationId = rand.nextInt(10000);
		try 
		{
			logger.info("Getting sendBuyerMail Process : Start");
			StringBuilder mailReq=new StringBuilder();
					
				mailReq.append("{");
				mailReq.append("\"request\": {");
				mailReq.append("\"header\": {");
				mailReq.append("\"soaCorrelationId\": \""+correlationId+"\",");
				mailReq.append("\"soaAppId\": \"POS\"");
				mailReq.append("},");
				mailReq.append("\"requestData\": {");
				mailReq.append("\"mailIdTo\": \""+mailTo+"\",");
				mailReq.append("\"mailSubject\": \""+username+ " here's your personal financial plan  \",");
				mailReq.append("\"fromName\": \"Maxlife Insurance\",");
				mailReq.append("\"attachments\": [{");
				mailReq.append("\"fileName\": \"lifegoalview.pdf\",");
				mailReq.append("\"byteArrayBase64\": \""+byteArray+"\"}");
				mailReq.append( "],");
				mailReq.append("\"isConsolidate\":false,");
				mailReq.append("\"isFileAttached\":true,");
				mailReq.append("\"fromEmail\": \"DoNotReply@maxlifeinsurance.com\",");
				mailReq.append("\"mailBody\": \"").append("<p>Hello "+username+",</p>\r\n" + 
						"<p>Congratulations on completing the need analysis journey. Hope we have been successful in helping you identify the way forward to achive your life's BIG GOAL.\r\n" + 
						"Please find attached your personalised financial plan summary basis the selected BIG goal and information shared with us.</p>\r\n" + 
						"<br>\r\n" + 
						"<p>Yours sincerely,</p>\r\n" + 
						"<p>Max Life Insurance </p>\r\n"+
						"<p>Link to our website : <a href=https://www.maxlifeinsurance.com>https://www.maxlifeinsurance.com/</a></p>"
						).append("\"");
				mailReq.append("}");
				mailReq.append("}");
				mailReq.append("}");
				httpCaller.callHttp(bean.getEmail(), mailReq, result);
				
		} catch (Exception e)
		{
			logger.info("Exception in sendMail  api :: " + e);
		}
		return result.toString();
				
				
	}

	@Override
	public  String camelCase(String inputString) {
		try {
			if(inputString!=null && !"".equalsIgnoreCase(inputString))
			{
			inputString = inputString.substring(0, 1).toUpperCase() + inputString.substring(1).toLowerCase();
			}
			
		} catch (Exception e) {
			inputString = "";
		}
		return inputString;
	}
	@SuppressWarnings("finally")
	@Override
	public String commaSepratedIndianFormat(String str)
	{
		String s = ""; 
        if(str.length()>1){
            s = str.substring(str.length()-1, str.length());
            str = str.substring(0, str.length()-1);
         }
        double d = 0;
        DecimalFormat twoDForm = new DecimalFormat(",##");
        try
        {
        	if((str == null) || (str.equals("")))
    		{
            	d  = 0;
    		}
            else
            	d = Double.parseDouble(str);
        }
        catch(Exception ex)
        {
        	logger.info("creating exception in commaSepratedIndianFormat "+ex);
        }
        return (twoDForm.format(d)+s);
       
	}
	
}
