package com.mli.bot.csg.serviceimpl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.commons.BeanProperty;
import com.mli.bot.csg.service.GetNonParDataService;
import com.mli.bot.csg.utils.CommonUtility;
@Service
public class GetNonParDataImpl implements GetNonParDataService{

	@Autowired
	private BeanProperty bean;

	@Autowired
	private CommonUtility commonUtility;
	
	String textResponse="";
	private static Logger logger = LogManager.getLogger(GetNonParDataImpl.class);
     
	@Override
	public String getNonPerData(String age, String term, String pocket, String plan)
	{
		logger.info("GetNonParDataImpl :: Start getNonPerData ...");
		try{
		String url=bean.getGetNonParData();
		textResponse = commonUtility.getParNonParAndUlipRequest(url, age, term, pocket, plan);
		}catch(Exception e) {
			logger.info("creating exception in  getNonPerData "+e);
		}
		logger.info("GetNonParDataImpl :: End getNonPerData ...");
		
		return textResponse;
	}
}
