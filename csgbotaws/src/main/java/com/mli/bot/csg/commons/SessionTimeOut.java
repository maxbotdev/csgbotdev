package com.mli.bot.csg.commons;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.service.NeedAnalysisService;
import com.mli.bot.csg.utils.CommonUtility;


@Service
public class SessionTimeOut {
	
	private static Logger logger = LogManager.getLogger(SessionTimeOut.class);	
	@Autowired
	private CalculateTimeDiff calculateTimeDiff;
	
	@Autowired
	CommonUtility commonUtility;
	@Autowired
	NeedAnalysisService needAnalysisService;
	
	
	
	/**
	 * @param externalMap
	 * Used for removing sessions from cache
	 */
	public void cashRemovetimeout(Map<String, Map<String,String>> externalMap, Map<String, Map<String,String>> questionAswerMap,
			Map<String, Map<String,String>> response ,Map<String,String> tokenCache)
	{
		logger.info("SessionTimeOut cashRemovetimeout ..start");
		
		setTokenInCachse(tokenCache);
		
		try 
		{
		for(Map.Entry<String, Map<String, String>> entry : externalMap.entrySet())
		{
			String keytoremove = entry.getKey();
			Map<String,String> valueMap = entry.getValue();
			
			for(Map.Entry<String,String> getKeyValueMap : valueMap.entrySet())
			{
				logger.info("Loop starting...");
				if(getKeyValueMap.getKey().contains(keytoremove))
				{
					logger.info("if condition success..");
					String logntimevalue = getKeyValueMap.getValue();
					long diffminutes = calculateTimeDiff.timeDiffInMinute(commonUtility.getCurrentDateAndTime(), logntimevalue);
					logger.info(commonUtility.getCurrentDateAndTime() +" " +logntimevalue);
					logger.info("diffminutes "+diffminutes);
					if(diffminutes >=2)
					{
						logger.info("callArciseServices......call");
						needAnalysisService.callArciseServices(keytoremove.replace("sessionRemove",""),"Sechedular");
						externalMap.remove(keytoremove);
						questionAswerMap.remove(keytoremove);
						response.remove(keytoremove);
						externalMap.remove(keytoremove+"Msg");
						logger.info("finish time out ..");
					}
					break;
				}
			}
			
		}
		}catch(Exception e)
		{
			logger.error("creating exception while going to session time out in SessionTimeOut :: cashRemovetimeout  "+e);
			
		}
		logger.info("SessionTimeOut cashRemovetimeout ..End");
	}
	

	private void setTokenInCachse(Map<String,String> tokenCache) 
	{
		logger.info("start setTokenInCachse .. ");
		try
		{
			
		String token=needAnalysisService.getArciseToken();
		System.out.println("token"+token);
		
	    tokenCache.put("token",token);
	    logger.info("End setTokenInCachse .. ");
		}catch(Exception e) 
		{
		logger.error("creating exception in Session Time Out while getting token.."+e);	
		}
	}
}
