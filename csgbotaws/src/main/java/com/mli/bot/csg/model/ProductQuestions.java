package com.mli.bot.csg.model;

import java.util.Map;

public interface ProductQuestions {

	
	public String productQuestions(Map<String,Map<String,String>> externalMap, String sessionId, String action);
}
