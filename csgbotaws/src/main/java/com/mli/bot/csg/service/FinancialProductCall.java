package com.mli.bot.csg.service;

import java.util.Map;

public interface FinancialProductCall {
	public String financialProductCall(Map<String, Map<String, String>> externalMap, String sessionId, String action, String questions);

}
