package com.mli.bot.csg.handlerinterfaceimpl;

import org.springframework.stereotype.Component;

import com.mli.bot.csg.exceptions.GenericCustomException;
import com.mli.bot.csg.handlerinterface.BasicRequestBuilder;
import com.mli.bot.csg.request.InternalBasicRequest;
import com.mli.bot.csg.request.WebhookRequest;

@Component
public class BasicRequestBuilderImpl implements BasicRequestBuilder
{
	public InternalBasicRequest getBasicRequest(WebhookRequest  webhookRequest) throws GenericCustomException{
		
		InternalBasicRequest internalBasicRequest=new InternalBasicRequest();
		try
		{
		internalBasicRequest.setAction(webhookRequest.getQueryResult().getAction());
		internalBasicRequest.setQueryText(webhookRequest.getQueryResult().getQueryText());
		internalBasicRequest.setSession(webhookRequest.getSession());
		internalBasicRequest.getParameters().setQuestions(webhookRequest.getQueryResult().getParameters().getQuestions());
		internalBasicRequest.getParameters().setAnswer(webhookRequest.getQueryResult().getParameters().getAnswer());
		}
		catch(Exception ec)
		{
			throw new GenericCustomException(ec.getMessage(), ec.getLocalizedMessage(), null);
		}
		return internalBasicRequest;
	}
}
