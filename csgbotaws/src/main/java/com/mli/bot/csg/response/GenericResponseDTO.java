package com.mli.bot.csg.response;

public class GenericResponseDTO 
{

	private String speech;
    private String displayText;
    private InnerData data;
    
	public GenericResponseDTO(String speech, String displayText, InnerData data) {
		super();
		this.speech = speech;
		this.displayText = displayText;
		this.data = data;
	}

	public String getSpeech() {
		return speech;
	}
	
	public void setSpeech(String speech) {
		this.speech = speech;
	}
	
	public String getDisplayText() {
		return displayText;
	}
	
	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}
	
	public InnerData getData() {
		return data;
	}
	
	public void setData(InnerData data) {
		this.data = data;
	}
	
	
}
