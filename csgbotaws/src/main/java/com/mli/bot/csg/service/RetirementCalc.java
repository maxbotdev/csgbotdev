package com.mli.bot.csg.service;

public interface RetirementCalc 
{
	public String retirementCalc(String age, String retirePlan, String monthlyExp, String expAfterRetire, 
			String inflationRate, String saving, String expectedRetSaving);

}
