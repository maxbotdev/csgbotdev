package com.mli.bot.csg.response;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.utils.CommonUtility;
@Service
public class ProductDetailsResponse {
	String speech="";
	@Autowired
	private CommonUtility commonUtility;
	
	public String productResponse(Map<String,Map<String,String>> externalMap, String sessionId)
	{
		speech=externalMap.get(sessionId+"Msg").get("ProductRecoMsg");
		StringBuilder sb = commonUtility.getButtonsResponse(speech);
		StringBuilder sb2=getDyanamicResponse(externalMap,sessionId);
		sb.append(sb2.toString());
		StringBuilder sb3 = commonUtility.addFinalRespone();
		sb.append(sb3.toString());
		return sb.toString();
	}

	private StringBuilder getDyanamicResponse(Map<String, Map<String, String>> externalMap, String sessionId) {
       
    
		String productDetails = externalMap.get(sessionId+"ProductMsg").get("ProductDetailsQuestion")+"";
		 StringBuilder sb2= commonUtility.makeDyanamicButton(productDetails, "");
		 sb2.append(commonUtility.makeDyanamicButton("Know More", ""));
		 sb2.append(commonUtility.makeDyanamicButton("Get Quote", ""));
		 sb2=commonUtility.removeLastCharcter(sb2);
		return sb2;
	}
}
