package com.mli.bot.csg.response;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.utils.CommonUtility;

@Service
public class FetchGoalResponse 
{
	@Autowired
	private CommonUtility commonUtility;
	
	String speech="";
	public String fetchGoalResponse(Map<String,Map<String,String>> externalMap, String sessionId, String speech)
	{
		StringBuilder sb = commonUtility.getButtonsResponse(speech);
		StringBuilder sb2=getDyanamicResponse(externalMap,sessionId);
		sb.append(sb2.toString());
		StringBuilder sb3 =commonUtility.addFinalRespone();
		sb.append(sb3.toString());
		return sb.toString();
	}
	private StringBuilder getDyanamicResponse(Map<String, Map<String, String>> externalMap, String sessionId) {
      StringBuilder sb2= new StringBuilder();
		String lifeGoal = externalMap.get(sessionId).get("Goals")+"";
		String [] str = lifeGoal.split(",");
		int sizeGoal = str.length;
		int count = 0;
		for(int i=0; i<str.length; i++)
		{
			count++;
			if(sizeGoal!=count){
				sb2.append("	        {	");
				sb2.append("	          \"text\": \" "+str[i]+"\",	");
				sb2.append("	          \"postback\": \""+str[i]+"\",	");
				sb2.append("	          \"link\": \"\"	");
				sb2.append("	        },	");
			}
			else{
				sb2.append("	        {	");
				sb2.append("	          \"text\": \" "+str[i]+"\",	");
				sb2.append("	          \"postback\": \""+str[i]+"\",	");
				sb2.append("	          \"link\": \"\"	");
				sb2.append("	        }	");
			}
		}
		
		return sb2;
	}
}
