package com.mli.bot.csg.service;

public interface GetParDataService {

	public String getParData(String age,String term,String pocket,String plan);
}
