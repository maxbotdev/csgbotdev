package com.mli.bot.csg.modelimpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.model.UserQuestions;
import com.mli.bot.csg.service.UserQuestionsCall;

@Service
public class UserQuestionsImpl implements UserQuestions
{
	@Autowired
	private UserQuestionsCall userQuestionsCall;
	static Map<String,Map<String,String>> questionMap = new ConcurrentHashMap<>();
	@Override
	public Map<String,Map<String,String>> userQuestions(Map<String, Map<String, String>> externalMap, String sessionId, String action, String questionsId) 
	{
		StringBuilder sb = new StringBuilder();
		Map<String,String> map = new LinkedHashMap<>();
		String [] goalSplit = questionsId.split("\\$\\$");
		
		String actionName;
		
		for(int i=0; i<goalSplit.length; i++)
		{
			actionName = goalSplit[i];
			switch(actionName)
			{
			case "Protection":
				actionName="PF";
			break;
			case "Savings/Wealth":
				actionName="RS";
			break;
			case "Child Education":
				actionName="CE";
			break;
			case "Legacy":
				actionName="LG";
			break;
			case "Retirement":
				actionName="ER";
			break;
			case "Child Marriage":
				actionName="CM";
			break;
			default:
				actionName="default";
			
			}
			if(!"default".equalsIgnoreCase(actionName))
			{
				sb.append(actionName+",");
				String questions = userQuestionsCall.userQuestionsCall(externalMap, sessionId, actionName, questionsId);
				String[] questionsArray = questions.split("\",");
				for(int j=0; j<questionsArray.length; j++)
				{
					String str=questionsArray[j];
					String[] strArray =str.split("\\$");
					str=str.replaceAll("[\\[\\]]", "");
					map.put(strArray[1], str);// changes by adit
				}
				questionMap.put(sessionId, map);
				externalMap.get(sessionId).put("Identifier", "Exists");
			}
		}
		externalMap.get(sessionId).put("Goals", sb.toString());
		return questionMap;
	}
}
