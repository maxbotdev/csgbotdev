package com.mli.bot.csg.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mli.bot.csg.beans.ArcisServicesApiBean;
import com.mli.bot.csg.commons.BeanProperty;

@Component
public class ArciseServicesHelper {
	
	private  Logger logger = LogManager.getLogger(ArciseServicesHelper.class);
	
	@Autowired
	BeanProperty beanProperty;
	
	@Autowired
	ResourceCloser resourceCloser;
	
	@Autowired CommonUtility commonUtility;
	
	public  String callgetTokenApi() {
	    String response = null;
	    logger.info("callgetTokenApi start ..");    
	    HttpURLConnection httpUrlConnection = null;
	    DataOutputStream dos = null;
	    InputStream is = null;
	    String params  = "grant_type="+beanProperty.getGrantType()+"&username="+beanProperty.getArcisesUserName()+"&password="+beanProperty.getArcisesAuth()+"";
	    try
	    {
	        URL url = new URL(beanProperty.getArcisesTokenUrl());
	        httpUrlConnection = (HttpURLConnection)url.openConnection();
	        httpUrlConnection.setDoInput(true);
	        httpUrlConnection.setDoOutput(true);
	        httpUrlConnection.setConnectTimeout(2000);
	        httpUrlConnection.setRequestMethod("POST");
	        httpUrlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        httpUrlConnection.setRequestProperty("charset", "utf-8");
	        httpUrlConnection.setRequestProperty("Content-Length", Integer.toString(params.getBytes().length));
	        dos = new DataOutputStream(httpUrlConnection.getOutputStream());
	        dos.writeBytes(params);
	        dos.flush();
	        is = httpUrlConnection.getInputStream();
	        Scanner scanner = new Scanner(is).useDelimiter("\\A");
	        response = scanner.hasNext() ? scanner.next() : null;
	        logger.info("callgetTokenApi Api Response "+response);
	        logger.info("ccallgetTokenApi End ...");   
	      }catch(Exception e)
	      {
	    	  logger.info("creating exception in callgetTokenApi "+e);
	      }
	       finally 
	      {
		    resourceCloser.closeResourceOfHttpCall(httpUrlConnection, null, null,is,dos);
	      }
	      return response;
	     }
	
	
	public  String  callAddDataApi(StringBuilder requestData,String token,String sourceName)
	{
		logger.info("callAddDataApi start ...with sourceName "+sourceName);
		
		logger.info("callAddDataApi Request : "+requestData+"  token "+token);
		HttpURLConnection httpConnection = null;
		OutputStreamWriter outputStreamWriter = null;
		BufferedReader bufferedReaderResponse = null;
		StringBuilder dataApiResult=new StringBuilder();
		try
		{
			String outputResponse;
			URL url = new URL(beanProperty.getAddDataArcisesUrl());
			httpConnection = (HttpURLConnection) url.openConnection();
			httpConnection.setRequestProperty("Authorization","bearer "+token);
			HttpsURLConnection.setFollowRedirects(true);
			httpConnection.setDoInput(true);
			httpConnection.setConnectTimeout(2000);
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("POST");
			httpConnection.setRequestProperty("Content-Type", "application/json");
			outputStreamWriter = new OutputStreamWriter(httpConnection.getOutputStream());
			outputStreamWriter.write(requestData.toString());
			outputStreamWriter.flush();

			int responseCode = httpConnection.getResponseCode();
			
			if (responseCode == 200) {
				bufferedReaderResponse = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
			} else {
				bufferedReaderResponse = new BufferedReader(new InputStreamReader(httpConnection.getErrorStream()));
			}
			while ((outputResponse = bufferedReaderResponse.readLine()) != null)
			{
				dataApiResult.append(outputResponse);
			}
			logger.info("callAddDataApi Response : "+dataApiResult);
			logger.info("callAddDataApi End ...");
		}
		catch(Exception e)
		{
			logger.info("creating Exception in callAddDataApi "+e);
		}
		finally
		{
		 resourceCloser.closeResourceOfHttpCall(httpConnection, outputStreamWriter, bufferedReaderResponse,null,null);
		}
		return dataApiResult.toString();
	}
	
	public String arciseAddData( String token,ArcisServicesApiBean bean,String sourceName)
	{
		
		logger.info("arciseAddData start .. with sourceName "+sourceName);
		String response=null;
		StringBuilder request=new StringBuilder();
		try
		{
		request.append("{");
		request.append("\"campaign_Name\": \""+commonUtility.nullThenBlank(bean.getCampaignName().trim())+"\",");
		request.append("\"source_Number\": \""+commonUtility.nullThenBlank(bean.getSourceNumber().trim())+"\",");
		request.append("\"sub_Source_Number\": \""+commonUtility.nullThenBlank(bean.getSubsourceNumber().trim())+"\",");
		request.append("\"name\": \""+commonUtility.nullThenBlank(bean.getName().trim())+"\",");
		request.append("\"policy_Number\": \""+commonUtility.nullThenBlank(bean.getPolicyNumber().trim())+"\",");
		request.append("\"registered_Mobile_No\": \""+commonUtility.nullThenBlank(bean.getRegisteredMobileNo().trim())+"\",");
		request.append("\"email\": \""+commonUtility.nullThenBlank(bean.getEmail())+"\",");
		request.append("\"Customer_Intraction_Date\": \""+commonUtility.nullThenBlank(bean.getCustomerIntractionDate())+"\",");
		request.append("\"age\": \""+commonUtility.nullThenBlank(bean.getAge())+"\",");
		request.append("\"income\": \""+commonUtility.nullThenBlank(bean.getIncome())+"\",");
		request.append("\"L2BI_Name\": \""+commonUtility.nullThenBlank(bean.getL2BIName())+"\",");
		request.append("\"L2BI_Age\": \""+commonUtility.nullThenBlank(bean.getL2BIAge())+"\",");
		request.append("\"education_Cost\": \""+commonUtility.nullThenBlank(bean.getEducationCost())+"\",");
		request.append("\"investment_Risk_Preference\": \""+commonUtility.nullThenBlank(bean.getInvestmentRiskPreference())+"\",");
		request.append("\"qualification_Stream_Chosen\": \""+commonUtility.nullThenBlank(bean.getQualificationStreamChosen())+"\",");
		request.append("\"current_Saving_Amount\": \""+commonUtility.nullThenBlank(bean.getCurrentSavingAmount())+"\",");
		request.append("\"monthly_Saving\": \""+commonUtility.nullThenBlank(bean.getMonthlySaving())+"\",");
		request.append("\"saving_Till_Date\": \""+commonUtility.nullThenBlank(bean.getSavingTillDate())+"\",");
		request.append("\"current_HouseHold_Expence\": \""+commonUtility.nullThenBlank(bean.getCurrentHouseHoldExpence())+"\",");
		request.append("\"expected_Monthly_Expence_After_Retirement\": \""+commonUtility.nullThenBlank(bean.getExpectedMonthlyExpenceAfterRetirement())+"\",");
		request.append("\"premium_Amount\": \""+commonUtility.nullThenBlank(bean.getPremiumAmount())+"\",");
		request.append("\"age_CustomerNeedMoney\": \""+commonUtility.nullThenBlank(bean.getAgeCustomerNeedMoney())+"\",");
		request.append("\"expected_Maturity_Amount\": \""+commonUtility.nullThenBlank(bean.getExpectedMaturityAmount())+"\",");
		request.append("\"product_Recommended\": \""+commonUtility.nullThenBlank(bean.getProductRecommended())+"\",");
		request.append("\"current_Loan_Amount\": \""+commonUtility.nullThenBlank(bean.getCurrentLoanAmount())+"\",");
		request.append("\"savings_Planned_To_Do\": \""+commonUtility.nullThenBlank(bean.getSavingsPlannedToDo())+"\",");
		request.append("\"life_Insurance_Cover\": \""+commonUtility.nullThenBlank(bean.getLifeInsuranceCover())+"\",");
		request.append("\"protection_Cover_Needed\": \""+commonUtility.nullThenBlank(bean.getProtectionCoverNeeded())+"\",");
		request.append("\"additional_Cover_Needed\": \""+commonUtility.nullThenBlank(bean.getAdditionalCoverNeeded())+"\"");
		request.append("}");
		
		
		response = callAddDataApi(request, token,sourceName);
		
		}catch(Exception e)
		{
		 logger.info("creating exception in arciseAddData "+e);
		}
		logger.info("arciseAddData End .. ");
		return response;
	}
	
}
