package com.mli.bot.csg.commons;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanProperty 
{
	
	@Value("${env.soaUserId}")
	private String soaUserId;
	
	@Value("${env.soaPassword}")
	private String soaPassword;
	
	@Value("${env.adoptionlogs}")
	private String adoptionlogs;
		
	@Value("${env.getMessageAPI}")
	private String getMessageAPI;
	
	@Value("${env.getNonParData}")
	private String getNonParData;
	
	@Value("${env.getParData}")
	private String getParData;
	
	@Value("${env.getUlipData}")
	private String getUlipData;
	
	@Value("${env.getChildEduationPremium}")
	private String getChildEduationPremium;
	
	@Value("${env.session.timeout.message}")
	private String sessionTimeoutMessage;
	
	@Value("${env.dev.mode}")
	private String devMode;
	
	@Value("${env.client.region}")
	private String clientRegion;
	
	@Value("${env.s3bucket.name}")
	private String s3bucketName;
	
	@Value("${env.secret.access.key}")
	private String secretAccessKey;
	
	@Value("${env.access.key.id}")
	private String accessKeyId;
	
	@Value("${env.getChildMarriagePremium}")
	private String getChildMarriagePremium;
	
	@Value("${env.email}")
	private String email;
	
	@Value("${env.getHappinessAndWorryFactors}")
	private String getHappinessAndWorryFactors;
	
	@Value("${env.getUserFinancialGoals}")
	private String getUserFinancialGoals;
	
	@Value("${env.getProductDetail}")
	private String getProductDetail;
	
	@Value("${env.getProtectionPremium}")
	private String getProtectionPremium;
	
	@Value("${env.getSavingPremium}")
	private String getSavingPremium;
	
	@Value("${env.getRetirementPremium}")
	private String getRetirementPremium;
	
	@Value("${env.getQuestions}")
	private String getQuestions;
	
	@Value("${env.getPdfPath}")
	private String getPdfPath;
	
	@Value("${env.arcises.token.url}")
	private String arcisesTokenUrl;
	@Value("${env.grant.type}")
	private String grantType;
	@Value("${env.arcises.username}")
	private String arcisesUserName;
	@Value("${env.arcises.auth}")
	private String arcisesAuth;
	@Value("${env.add.data.arcises.url}")
    private String addDataArcisesUrl; 
	@Value("${env.campaign.name}")
	private String campaignName;
	@Value("${env.source.number}")
	private String sourceNumber;
	@Value("${env.sub.sourcenumber}")
	private String subsourceNumber;
	
	
	
	
	
	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getSourceNumber() {
		return sourceNumber;
	}

	public void setSourceNumber(String sourceNumber) {
		this.sourceNumber = sourceNumber;
	}

	public String getArcisesTokenUrl() {
		return arcisesTokenUrl;
	}

	public void setArcisesTokenUrl(String arcisesTokenUrl) {
		this.arcisesTokenUrl = arcisesTokenUrl;
	}

	public String getGrantType() {
		return grantType;
	}

	public String getSubsourceNumber() {
		return subsourceNumber;
	}

	public void setSubsourceNumber(String subsourceNumber) {
		this.subsourceNumber = subsourceNumber;
	}

	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}

	public String getArcisesUserName() {
		return arcisesUserName;
	}

	public void setArcisesUserName(String arcisesUserName) {
		this.arcisesUserName = arcisesUserName;
	}

	public String getArcisesAuth() {
		return arcisesAuth;
	}

	public void setArcisesAuth(String arcisesAuth) {
		this.arcisesAuth = arcisesAuth;
	}

	public String getAddDataArcisesUrl() {
		return addDataArcisesUrl;
	}

	public void setAddDataArcisesUrl(String addDataArcisesUrl) {
		this.addDataArcisesUrl = addDataArcisesUrl;
	}

	public String getSoaUserId() {
		return soaUserId;
	}

	public void setSoaUserId(String soaUserId) {
		this.soaUserId = soaUserId;
	}

	public String getSoaPassword() {
		return soaPassword;
	}

	public void setSoaPassword(String soaPassword) {
		this.soaPassword = soaPassword;
	}

	public String getAdoptionlogs() {
		return adoptionlogs;
	}

	public void setAdoptionlogs(String adoptionlogs) {
		this.adoptionlogs = adoptionlogs;
	}

	public String getGetMessageAPI() {
		return getMessageAPI;
	}

	public void setGetMessageAPI(String getMessageAPI) {
		this.getMessageAPI = getMessageAPI;
	}

	public String getGetNonParData() {
		return getNonParData;
	}

	public void setGetNonParData(String getNonParData) {
		this.getNonParData = getNonParData;
	}

	public String getGetParData() {
		return getParData;
	}

	public void setGetParData(String getParData) {
		this.getParData = getParData;
	}

	public String getGetUlipData() {
		return getUlipData;
	}

	public void setGetUlipData(String getUlipData) {
		this.getUlipData = getUlipData;
	}

	public String getGetChildEduationPremium() {
		return getChildEduationPremium;
	}

	public void setGetChildEduationPremium(String getChildEduationPremium) {
		this.getChildEduationPremium = getChildEduationPremium;
	}

	public String getSessionTimeoutMessage() {
		return sessionTimeoutMessage;
	}

	public void setSessionTimeoutMessage(String sessionTimeoutMessage) {
		this.sessionTimeoutMessage = sessionTimeoutMessage;
	}

	public String getDevMode() {
		return devMode;
	}

	public void setDevMode(String devMode) {
		this.devMode = devMode;
	}

	public String getClientRegion() {
		return clientRegion;
	}

	public void setClientRegion(String clientRegion) {
		this.clientRegion = clientRegion;
	}

	public String getS3bucketName() {
		return s3bucketName;
	}

	public void setS3bucketName(String s3bucketName) {
		this.s3bucketName = s3bucketName;
	}

	public String getSecretAccessKey() {
		return secretAccessKey;
	}

	public void setSecretAccessKey(String secretAccessKey) {
		this.secretAccessKey = secretAccessKey;
	}

	public String getAccessKeyId() {
		return accessKeyId;
	}

	public void setAccessKeyId(String accessKeyId) {
		this.accessKeyId = accessKeyId;
	}

	public String getGetChildMarriagePremium() {
		return getChildMarriagePremium;
	}

	public void setGetChildMarriagePremium(String getChildMarriagePremium) {
		this.getChildMarriagePremium = getChildMarriagePremium;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGetHappinessAndWorryFactors() {
		return getHappinessAndWorryFactors;
	}

	public void setGetHappinessAndWorryFactors(String getHappinessAndWorryFactors) {
		this.getHappinessAndWorryFactors = getHappinessAndWorryFactors;
	}

	public String getGetUserFinancialGoals() {
		return getUserFinancialGoals;
	}

	public void setGetUserFinancialGoals(String getUserFinancialGoals) {
		this.getUserFinancialGoals = getUserFinancialGoals;
	}

	public String getGetProductDetail() {
		return getProductDetail;
	}

	public void setGetProductDetail(String getProductDetail) {
		this.getProductDetail = getProductDetail;
	}

	public String getGetProtectionPremium() {
		return getProtectionPremium;
	}

	public void setGetProtectionPremium(String getProtectionPremium) {
		this.getProtectionPremium = getProtectionPremium;
	}

	public String getGetSavingPremium() {
		return getSavingPremium;
	}

	public void setGetSavingPremium(String getSavingPremium) {
		this.getSavingPremium = getSavingPremium;
	}

	public String getGetRetirementPremium() {
		return getRetirementPremium;
	}

	public void setGetRetirementPremium(String getRetirementPremium) {
		this.getRetirementPremium = getRetirementPremium;
	}

	public String getGetQuestions() {
		return getQuestions;
	}

	public void setGetQuestions(String getQuestions) {
		this.getQuestions = getQuestions;
	}

	public String getGetPdfPath() {
		return getPdfPath;
	}

	public void setGetPdfPath(String getPdfPath) {
		this.getPdfPath = getPdfPath;
	}
	}
