package com.mli.bot.csg.model;

import java.util.Map;

public interface UserQuestions 
{
	public Map<String,Map<String,String>> userQuestions(Map<String, Map<String, String>> externalMap, String sessionId, String action, String questions); 

}
