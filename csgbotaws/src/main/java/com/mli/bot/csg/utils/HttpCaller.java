package com.mli.bot.csg.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ad01084
 *it is used to make all HTTP calls
 */
@Component
public class HttpCaller 
{
	private static Logger logger = LogManager.getLogger(HttpCaller.class);
	@Autowired
	ResourceCloser resourceCloser;
	/**
	 * @param extURL
	 * @param requestData
	 * @param result
	 */
	public void callHttp(
			String extURL,
			StringBuilder requestData, 
			StringBuilder result
			)
	{
		HttpURLConnection httpUrlConnection = null;
		OutputStreamWriter outputStreamWriter = null;
		BufferedReader bufferedReader = null;
		try
		{
			logger.info("API REQUEST -- " + requestData.toString());
			String output;
			URL url = new URL(extURL);
			httpUrlConnection = (HttpURLConnection) url.openConnection();
			HttpsURLConnection.setFollowRedirects(true);
			httpUrlConnection.setDoInput(true);
			httpUrlConnection.setDoOutput(true);
			httpUrlConnection.setRequestMethod("POST");
			httpUrlConnection.setRequestProperty("Content-Type", "application/json");
			outputStreamWriter = new OutputStreamWriter(httpUrlConnection.getOutputStream());
			outputStreamWriter.write(requestData.toString());
			outputStreamWriter.flush();

			int apiResponseCode = httpUrlConnection.getResponseCode();
			if (apiResponseCode == 200) {
				bufferedReader = new BufferedReader(new InputStreamReader(httpUrlConnection.getInputStream()));
			} else {
				bufferedReader = new BufferedReader(new InputStreamReader(httpUrlConnection.getErrorStream()));
			}
			while ((output = bufferedReader.readLine()) != null) {
				result.append(output);
			}
			logger.info("API RESPONSE -- " + result.toString());
		}
		catch(Exception e)
		{
			logger.info("Exception Occoured While Calling API's " + e);
		}
		finally
		{
			resourceCloser.closeResourceOfHttpCall(httpUrlConnection, outputStreamWriter, bufferedReader,null,null);
		}
	}
}
