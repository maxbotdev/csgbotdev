package com.mli.bot.csg.service;

import java.io.ByteArrayOutputStream;
import java.util.Map;

import com.mli.bot.csg.beans.FinalPlanDetailBean;

public interface PDFCreationService {
	
	public  void createPdf(String username,String pathwithName,Map externalMap,String sessionId,String planName);
	public ByteArrayOutputStream textPdfCreator(String username, Map externalMap,String sessionId,String planNameForImage);
	
	public FinalPlanDetailBean finalPlanDetail(Map<String, String> plan);
}
