package com.mli.bot.csg.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mli.bot.csg.commons.BeanProperty;
import com.mli.bot.csg.service.ChildEducationCalculator;

@Service
public class ChildEducationCalculatorImpl implements ChildEducationCalculator  
{
	@Autowired
	private BeanProperty bean;
	String resultChildEducation;
	@Override
	public String childEducationCalculator(String age, String graduation, String studyType, String country, String currentSavings, 
			String childAge,String inflationRate, String rateReturnInvestment) 
	{
		String url=bean.getGetChildEduationPremium();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"age\":\""+age+"\",	");
		sb.append("	  \"graduation\":\""+graduation+"\",	");
		sb.append("	  \"studyType\":\""+studyType+"\",	");
		sb.append("	  \"Country\":\""+country+"\",	");
		sb.append("	  \"currentSavings\":\""+currentSavings+"\",	");
		sb.append("	  \"childAge\":\""+childAge+"\",	");
		sb.append("	  \"inflationRate\":\""+inflationRate+"\",	");
		sb.append("	  \"rateReturnInvestment\":\""+rateReturnInvestment+"\"	");
		sb.append("	}	");
		HttpEntity<String> entity=new HttpEntity<>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			resultChildEducation = response.getBody();
		
		}
		return resultChildEducation;
	}
}
