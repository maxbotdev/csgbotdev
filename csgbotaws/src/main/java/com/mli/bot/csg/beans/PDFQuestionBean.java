package com.mli.bot.csg.beans;

import org.springframework.stereotype.Component;

/**
 * @author Adit
 *
 */
@Component
public class PDFQuestionBean 
{
private	String q1=""; 
private	String q2=""; 
private	String q3=""; 
private	String q4=""; 
private	String q5=""; 
private	String q8=""; 
private	String q9=""; 
private	String q10=""; 
private	String q11=""; 
private	String q12=""; 
private	String q13=""; 
private	String q25=""; 
private	String q26=""; 
private	String q27=""; 
private	String q7=""; 
private	String q14=""; 
private	String q16=""; 
private	String q15=""; 
private	String q17=""; 
private	String q18=""; 
private	String q19=""; 
private	String q20=""; 
private	String q24=""; 
private	String q21=""; 
private	String q22=""; 
private	String q23="";
public String getQ1() {
	return q1;
}
public void setQ1(String q1) {
	this.q1 = q1;
}
public String getQ2() {
	return q2;
}
public void setQ2(String q2) {
	this.q2 = q2;
}
public String getQ3() {
	return q3;
}
public void setQ3(String q3) {
	this.q3 = q3;
}
public String getQ4() {
	return q4;
}
public void setQ4(String q4) {
	this.q4 = q4;
}
public String getQ5() {
	return q5;
}
public void setQ5(String q5) {
	this.q5 = q5;
}
public String getQ8() {
	return q8;
}
public void setQ8(String q8) {
	this.q8 = q8;
}
public String getQ9() {
	return q9;
}
public void setQ9(String q9) {
	this.q9 = q9;
}
public String getQ10() {
	return q10;
}
public void setQ10(String q10) {
	this.q10 = q10;
}
public String getQ11() {
	return q11;
}
public void setQ11(String q11) {
	this.q11 = q11;
}
public String getQ12() {
	return q12;
}
public void setQ12(String q12) {
	this.q12 = q12;
}
public String getQ13() {
	return q13;
}
public void setQ13(String q13) {
	this.q13 = q13;
}
public String getQ25() {
	return q25;
}
public void setQ25(String q25) {
	this.q25 = q25;
}
public String getQ26() {
	return q26;
}
public void setQ26(String q26) {
	this.q26 = q26;
}
public String getQ27() {
	return q27;
}
public void setQ27(String q27) {
	this.q27 = q27;
}
public String getQ7() {
	return q7;
}
public void setQ7(String q7) {
	this.q7 = q7;
}
public String getQ14() {
	return q14;
}
public void setQ14(String q14) {
	this.q14 = q14;
}
public String getQ16() {
	return q16;
}
public void setQ16(String q16) {
	this.q16 = q16;
}
public String getQ15() {
	return q15;
}
public void setQ15(String q15) {
	this.q15 = q15;
}
public String getQ17() {
	return q17;
}
public void setQ17(String q17) {
	this.q17 = q17;
}
public String getQ18() {
	return q18;
}
public void setQ18(String q18) {
	this.q18 = q18;
}
public String getQ19() {
	return q19;
}
public void setQ19(String q19) {
	this.q19 = q19;
}
public String getQ20() {
	return q20;
}
public void setQ20(String q20) {
	this.q20 = q20;
}
public String getQ24() {
	return q24;
}
public void setQ24(String q24) {
	this.q24 = q24;
}
public String getQ21() {
	return q21;
}
public void setQ21(String q21) {
	this.q21 = q21;
}
public String getQ22() {
	return q22;
}
public void setQ22(String q22) {
	this.q22 = q22;
}
public String getQ23() {
	return q23;
}
public void setQ23(String q23) {
	this.q23 = q23;
}
@Override
public String toString() {
	return "PDF_Helper [q1=" + q1 + ", q2=" + q2 + ", q3=" + q3 + ", q4=" + q4 + ", q5=" + q5 + ", q8=" + q8 + ", q9="
			+ q9 + ", q10=" + q10 + ", q11=" + q11 + ", q12=" + q12 + ", q13=" + q13 + ", q25=" + q25 + ", q26=" + q26
			+ ", q27=" + q27 + ", q7=" + q7 + ", q14=" + q14 + ", q16=" + q16 + ", q15=" + q15 + ", q17=" + q17
			+ ", q18=" + q18 + ", q19=" + q19 + ", q20=" + q20 + ", q24=" + q24 + ", q21=" + q21 + ", q22=" + q22
			+ ", q23=" + q23 + "]";
}
}
