package com.mli.bot.csg.service;

import java.util.Map;

public interface CSGQuestionsCall 
{
	public String csgQuestionsServiceCall(Map<String,Map<String,String>> externalMap, String sessionId, String action);

}
