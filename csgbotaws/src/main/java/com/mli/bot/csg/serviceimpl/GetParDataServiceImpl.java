package com.mli.bot.csg.serviceimpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.commons.BeanProperty;
import com.mli.bot.csg.service.GetParDataService;
import com.mli.bot.csg.utils.CommonUtility;

@Service
public class GetParDataServiceImpl implements GetParDataService{

	private static Logger logger = LogManager.getLogger(GetParDataServiceImpl.class);
	@Autowired
	private BeanProperty bean;
	
	@Autowired
	private CommonUtility commonUtility;
	
	String textResponse;
	@Override
	public String getParData(String age, String term, String pocket, String plan) {
		
		logger.info("Start .. GetParDataServiceImpl :: GetParData ");
		try{
	    String url=bean.getGetParData();
	    textResponse = commonUtility.getParNonParAndUlipRequest(url, age, term, pocket, plan);
		}catch(Exception e) {
			logger.info("creating exception in  GetParDataServiceImpl "+e);
		}
		logger.info("End .. GetParDataServiceImpl :: GetParData ");
		
		return textResponse;
	}

}
