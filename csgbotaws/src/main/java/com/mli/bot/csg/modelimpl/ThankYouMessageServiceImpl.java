package com.mli.bot.csg.modelimpl;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.axiom.om.util.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.commons.BeanProperty;
import com.mli.bot.csg.model.ThankYouMessageModel;
import com.mli.bot.csg.serviceimpl.CommonServiceImpl;

@Service
public class ThankYouMessageServiceImpl implements ThankYouMessageModel{
	
	private static Logger logger = LogManager.getLogger(ThankYouMessageServiceImpl.class);

	@Autowired
	CommonServiceImpl cmmonServiceImpl;
	@Autowired
	private BeanProperty bean;
	String speech="";
	@Override
	public String thankYou(Map<String, Map<String, String>> externalMap, String sessionId, String mailTo ,Map questionAswerMap) {
			try {
				String username="";
				Map<String, String> newMap = (Map<String, String>) questionAswerMap.get(sessionId);
				for(Entry<String,String> DyanamicResponse:newMap.entrySet()) {
				String key=DyanamicResponse.getKey();
				String value=DyanamicResponse.getValue();
				 if("Q27".equalsIgnoreCase(key))
				{
					username=cmmonServiceImpl.camelCase(value);
					logger.info("User Name :: "+username);
				}
				}
			String fileName=sessionId.split("/")[4]+".pdf";
		    String workingDir =  bean.getGetPdfPath();
		    logger.info("Working dir ++++ "+workingDir);
		    logger.info("Path =+++=== "+workingDir+File.separator+fileName);
			String realFile=workingDir+File.separator+fileName;	
		    String[] str=mailTo.split(" ");
		    
     		byte[] bytedata=cmmonServiceImpl.convertPdfToByteArray(workingDir+File.separator+fileName);
     		String filedata=Base64.encode(bytedata);
	    	cmmonServiceImpl.sendMail(filedata,str[1],username);
	    	
	    	if (new File(realFile).exists())
	    	{
	    		boolean status=new File(realFile).delete();
	    		logger.info("Delete file status "+status);
	    	 }
	    	
	    	deleteS3Image(externalMap,workingDir,sessionId);
	    	
			}catch(Exception e) 
			{
				logger.info("exception in sending mail "+e);
			}
			 speech=externalMap.get(sessionId+"Msg").get("ThankyouMsg");
			 return speech;
		}
	
	private void deleteS3Image(Map<String, Map<String, String>> externalMap, String workingDir, String sessionId)
	{
		try
		{
		String s3Image=workingDir+File.separator+externalMap.get(sessionId+"S3").get("s3ImageName").split(" ")[3]+".png";
    	logger.info("S3 Image from external Map "+s3Image);
    	if (new File(s3Image).exists())
    	{
    		boolean status=new File(s3Image).delete();
    		logger.info("s3Image image file Delete status "+status);
    	 }
		}catch(Exception e)
		{
		 logger.info("creating exception while deleting s3 Image "+e);
		}
	}
	}


