package com.mli.bot.csg.handlerinterface;

import com.mli.bot.csg.request.WebhookRequest;

public interface HandlerFactoryPattern {
	
	public RequestResponseHandler  getHandlerObject(WebhookRequest request);
}
