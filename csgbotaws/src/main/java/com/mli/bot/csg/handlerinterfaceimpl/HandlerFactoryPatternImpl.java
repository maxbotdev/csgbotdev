package com.mli.bot.csg.handlerinterfaceimpl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mli.bot.csg.handlerinterface.BasicRequestBuilder;
import com.mli.bot.csg.handlerinterface.HandlerFactoryPattern;
import com.mli.bot.csg.handlerinterface.RequestResponseHandler;
import com.mli.bot.csg.request.WebhookRequest;
import com.mli.bot.csg.service.NeedAnalysisService;



@Component
public class HandlerFactoryPatternImpl implements HandlerFactoryPattern{


	@Autowired
	NeedAnalysisService needAnalysisService;
	
	@Autowired 
	BasicRequestBuilder basicRequestBuilder;  
	
	@Override
	public RequestResponseHandler getHandlerObject(WebhookRequest request) {
		
			return new WebHandler(request,needAnalysisService,basicRequestBuilder);
			
	}

	
}
