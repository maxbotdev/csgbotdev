package com.mli.bot.csg.service;

import java.util.Map;

public interface ProductDetails {
	public String productdetailsQuestion(Map<String,Map<String,String>> externalMap, String sessionId, String action);
}
