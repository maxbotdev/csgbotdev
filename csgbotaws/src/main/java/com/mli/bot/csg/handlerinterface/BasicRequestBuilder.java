package com.mli.bot.csg.handlerinterface;

import com.mli.bot.csg.exceptions.GenericCustomException;
import com.mli.bot.csg.request.InternalBasicRequest;
import com.mli.bot.csg.request.WebhookRequest;

public interface BasicRequestBuilder {

	public InternalBasicRequest getBasicRequest(WebhookRequest  webhookRequest) throws GenericCustomException;
	
}
