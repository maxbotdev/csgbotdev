package com.mli.bot.csg.calculatorimpl;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.calculator.ChildMarriage;
import com.mli.bot.csg.service.ChildMarriageCalculator;

@Service
public class ChildMarriageImpl implements ChildMarriage 
{
	@Autowired
	private ChildMarriageCalculator childMarriageCalculator; 
	@Override
	public String childMarriage(Map<String, Map<String, String>> questionAswerMap, String sessionId) 
	{
		String age="";
		String  marriageAge="";
		String neededMoney="";
		String asideMoney="";
		String inflationRate="";
		String expectSaving="";

		for(Entry<String,String> entry : questionAswerMap.get(sessionId).entrySet())
		{
			String key =entry.getKey();
			String value =entry.getValue();
			if("Q2".equalsIgnoreCase(key))
			{
				age=value;
			}
			else if("Q10".equalsIgnoreCase(key))
			{
				marriageAge=value;
			}
			else if("Q11".equalsIgnoreCase(key))
			{
				neededMoney=value;
			}
			else if("Q12".equalsIgnoreCase(key))
			{
				asideMoney=value;
			}
			else if("Q9".equalsIgnoreCase(key))
			{
				expectSaving=value;
				if("N".equalsIgnoreCase(expectSaving))
				{
					expectSaving="8";
				}
			}
			else if("Q8".equalsIgnoreCase(key))
			{
				inflationRate=value;
				if("N".equalsIgnoreCase(inflationRate))
				{
					inflationRate="8";
				}
			}
		}
		return childMarriageCalculator.childMarriageCalculator(age, marriageAge, neededMoney, asideMoney, expectSaving,inflationRate);
	}
}
