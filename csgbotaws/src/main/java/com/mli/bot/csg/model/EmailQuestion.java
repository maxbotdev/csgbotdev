package com.mli.bot.csg.model;

import java.util.Map;

public interface EmailQuestion {

	public String emailQuestion(Map<String,Map<String,String>> externalMap, String sessionId, String action, String questions);
	
}
