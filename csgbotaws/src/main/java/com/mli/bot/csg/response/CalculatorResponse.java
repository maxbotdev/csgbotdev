package com.mli.bot.csg.response;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.utils.CommonUtility;

@Service
public class CalculatorResponse 
{
	String speech="";
	
	@Autowired
	private CommonUtility commonUtility;
	
	public String calculatorResponse(Map<String,Map<String,String>> externalMap, String sessionId, String speech)
	{

		StringBuilder sb = commonUtility.getButtonsResponse(speech);
        StringBuilder sb2=getSb2Details(externalMap,sessionId);
        sb.append(sb2.toString());
		StringBuilder sb3 = commonUtility.addFinalRespone();
		sb.append(sb3.toString());
		return sb.toString();
	}

	private StringBuilder getSb2Details(Map<String,Map<String,String>> externalMap , String sessionId)
	{
		StringBuilder sb2= new StringBuilder();
		Map<String, String> newMap = externalMap.get(sessionId);
		String str1=newMap.get("protectionofmyfamily")+"";
		int var=0;
		if("null".equalsIgnoreCase(str1))
			str1="";
		else 
			var++;
		String str2=newMap.get("RegularSaving")+"";
		if("null".equalsIgnoreCase(str2))
			str2="";
		else
			var++;
		
		String str3=newMap.get("Retirement")+"";
		if("null".equalsIgnoreCase(str3))
			str3="";
		else 
			var++;
		
		String str4=newMap.get("ChildEducation")+"";
		if("null".equalsIgnoreCase(str4))
			str4="";
		
		else 
			var++;
		
		String str5=newMap.get("ChildMarriage")+"";
		if("null".equalsIgnoreCase(str5))
			str5="";
		else
			var++;
		String plan=str1+"#"+str2+"#"+str3+"#"+str4+"#"+str5;
		String [] planSplit = plan.split("#");
		int size= planSplit.length;
		int count=1;
		for (Map.Entry<String, String> entry : newMap.entrySet())
		{
			String key = entry.getKey();
			String value = entry.getValue();
			if("protectionofmyfamily".equalsIgnoreCase(key) || "RegularSaving".equalsIgnoreCase(key)
					||"Retirement".equalsIgnoreCase(key)||"ChildEducation".equalsIgnoreCase(key)||"ChildMarriage".equalsIgnoreCase(key))
			{
				count++;
				sb2.append("	        {	");
				sb2.append("	          \"text\": \" "+key+"\",	");
				sb2.append("	          \"postback\": \""+value+"\",	");
				sb2.append("	          \"link\": \"\"	");
				if(var!=1)
				{
					if(count!=size)
						sb2.append("	        },	");
					else
						sb2.append("	        }	");
				}
				else
					sb2.append("	        }	");
			}
		}
		return sb2;
	}
	
}	
	
	
	
	
	
	

