package com.mli.bot.csg.calculatorimpl;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.calculator.ChildEducation;
import com.mli.bot.csg.service.ChildEducationCalculator;

@Service
public class ChildEducationImpl implements ChildEducation 
{
	
	@Autowired
	private ChildEducationCalculator childEducationCalculator;

	@Override
	public String childEducation(Map<String, Map<String, String>> questionAswerMap, String sessionId)
	{
		String age="";
		String graduation="";
		String studyType="";
		String country="";
		String currentSavings="";
		String childAge="";
		String inflationRate="";
		String rateReturnInvestment="";
		for(Entry<String,String> entry : questionAswerMap.get(sessionId).entrySet())
		{
			String key =entry.getKey();
			String value =entry.getValue();
			if("Q2".equalsIgnoreCase(key))
			{
				age=value;
			}
			else if("Q3".equalsIgnoreCase(key))
			{
				graduation=value;
			}
			else if("Q4".equalsIgnoreCase(key))
			{
				studyType=value;
			}
			else if("Q5".equalsIgnoreCase(key))
			{
				country=value;
			}
			else if("Q6".equalsIgnoreCase(key))
			{
				currentSavings=value;
			}
			else if("Q7".equalsIgnoreCase(key))
			{
				childAge=value;
			}
			else if("Q8".equalsIgnoreCase(key))
			{
				inflationRate=value;
				if("N".equalsIgnoreCase(inflationRate))
				{
					inflationRate="8";
				}
			}
			else if("Q9".equalsIgnoreCase(key))
			{
				rateReturnInvestment=value;
				if("N".equalsIgnoreCase(rateReturnInvestment))
				{
					rateReturnInvestment="8";
				}
			}
		}
		return childEducationCalculator.childEducationCalculator(age, graduation, studyType, country, currentSavings, 
				childAge,inflationRate, rateReturnInvestment);
	}

}
