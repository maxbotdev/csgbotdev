package com.mli.bot.csg.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * @author ad01084
 * For closing resources write generic methods here
 *
 */
@Component
public class ResourceCloser {

	private static Logger logger = LogManager.getLogger(ResourceCloser.class);
	/**
	 * @param httpUrlConnection
	 * @param outputStreamWriter
	 * @param bufferedReader
	 * @param inputStream
	 * @param dataOutputStream
	 */
	public void closeResourceOfHttpCall(HttpURLConnection httpUrlConnection,OutputStreamWriter outputStreamWriter,BufferedReader bufferedReader,InputStream inputStream,DataOutputStream dataOutputStream)
	{
		try
		{
			if(outputStreamWriter!=null) 
			{
				outputStreamWriter.close();
				logger.debug("OutputStreamWriter Closed");
			}
			if(bufferedReader!=null)
			{
				bufferedReader.close();
				logger.debug("BufferedReader Closed");
			}
			if(httpUrlConnection!=null) 
			{
				httpUrlConnection.disconnect();
				logger.debug("HttpURLConnection Disconnected");
			}
			if(inputStream!=null) 
			{
				inputStream.close();
				logger.debug("inputStream closed");
			}
			if(dataOutputStream!=null) 
			{
				dataOutputStream.close();
				logger.debug("dataOutputStream closed");
			}
			
		}
		catch(Exception ex)
		{
			logger.error("Critical Scenario : Check on Priority As we face exception while closing the resource : "+ex);
		}
	}

}
