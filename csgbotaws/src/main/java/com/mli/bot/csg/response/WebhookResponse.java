package com.mli.bot.csg.response;

public class WebhookResponse implements GenericResponse  
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fulfillmentText;
    private WebhookResponsefulfillmentMsg[]    fulfillmentMessages;
    private InnerData payload;
    private String source = "java-webhook";


    
	/**
	 * @param speech
	 * @param displayText
	 * @param data
	 */
	public WebhookResponse(String fulfillmentMessage, String fulfillmentText,InnerData payload) {
		fulfillmentMessages=new WebhookResponsefulfillmentMsg[1];
		fulfillmentMessages[0]=new WebhookResponsefulfillmentMsg();
		fulfillmentMessages[0].getText().getText()[0]=fulfillmentMessage;
		this.fulfillmentText = fulfillmentText;
		this.payload=payload;
	}		

	
	public String getFulfillmentText() {
		return fulfillmentText;
	}


	public void setFulfillmentText(String fulfillmentText) {
		this.fulfillmentText = fulfillmentText;
	}


    public WebhookResponsefulfillmentMsg[] getFulfillmentMessages() {
		return fulfillmentMessages;
	}


	public void setFulfillmentMessages(WebhookResponsefulfillmentMsg[] fulfillmentMessages) {
		this.fulfillmentMessages = fulfillmentMessages;
	}


	public String getSource() {
        return source;
    }

	
	public InnerData getPayload() {
		return payload;
	}

	public void setPayload(InnerData payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		return "WebhookResponse [fulfillmentMessages=" + fulfillmentMessages + ", fulfillmentText=" + fulfillmentText + ", payload=" + payload + ", source="
				+ source + "]";
	}
	
}
