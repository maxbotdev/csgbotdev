package com.mli.bot.csg.beans;

import org.springframework.stereotype.Component;

/**
 * @author Adit
 */
@Component
public class BasicDetailQuesAnsBean 
{
	private String basicDetail;
	private String basicDetailQuesFirst ;
	private String basicDetailQuesSecond ;
	private String basicDetailQuesThird ;
	private String basicDetailQuesFour ;
	private String basicDetailQuesFive ;
	private String basicDetailQuesSix ;
	private String basicDetailQuesSeven ;
	private String basicDetailAnsFirst ;
	private String basicDetailAnsSecond ;
	private String basicDetailAnsThird ;
	private String basicDetailAnsFour ;
	private String basicDetailAnsFive ;
	private String basicDetailAnsSix;
	private String basicDetailAnsSeven;
	
	public String getBasicDetail() {
		return basicDetail;
	}
	public void setBasicDetail(String basicDetail) {
		this.basicDetail = basicDetail;
	}
	public String getBasicDetailQuesFirst() {
		return basicDetailQuesFirst;
	}
	public void setBasicDetailQuesFirst(String basicDetailQuesFirst) {
		this.basicDetailQuesFirst = basicDetailQuesFirst;
	}
	public String getBasicDetailQuesSecond() {
		return basicDetailQuesSecond;
	}
	public void setBasicDetailQuesSecond(String basicDetailQuesSecond) {
		this.basicDetailQuesSecond = basicDetailQuesSecond;
	}
	public String getBasicDetailQuesThird() {
		return basicDetailQuesThird;
	}
	public void setBasicDetailQuesThird(String basicDetailQuesThird) {
		this.basicDetailQuesThird = basicDetailQuesThird;
	}
	public String getBasicDetailQuesFour() {
		return basicDetailQuesFour;
	}
	public void setBasicDetailQuesFour(String basicDetailQuesFour) {
		this.basicDetailQuesFour = basicDetailQuesFour;
	}
	public String getBasicDetailQuesFive() {
		return basicDetailQuesFive;
	}
	public void setBasicDetailQuesFive(String basicDetailQuesFive) {
		this.basicDetailQuesFive = basicDetailQuesFive;
	}
	public String getBasicDetailQuesSix() {
		return basicDetailQuesSix;
	}
	public void setBasicDetailQuesSix(String basicDetailQuesSix) {
		this.basicDetailQuesSix = basicDetailQuesSix;
	}
	public String getBasicDetailQuesSeven() {
		return basicDetailQuesSeven;
	}
	public void setBasicDetailQuesSeven(String basicDetailQuesSeven) {
		this.basicDetailQuesSeven = basicDetailQuesSeven;
	}
	public String getBasicDetailAnsFirst() {
		return basicDetailAnsFirst;
	}
	public void setBasicDetailAnsFirst(String basicDetailAnsFirst) {
		this.basicDetailAnsFirst = basicDetailAnsFirst;
	}
	public String getBasicDetailAnsSecond() {
		return basicDetailAnsSecond;
	}
	public void setBasicDetailAnsSecond(String basicDetailAnsSecond) {
		this.basicDetailAnsSecond = basicDetailAnsSecond;
	}
	public String getBasicDetailAnsThird() {
		return basicDetailAnsThird;
	}
	public void setBasicDetailAnsThird(String basicDetailAnsThird) {
		this.basicDetailAnsThird = basicDetailAnsThird;
	}
	public String getBasicDetailAnsFour() {
		return basicDetailAnsFour;
	}
	public void setBasicDetailAnsFour(String basicDetailAnsFour) {
		this.basicDetailAnsFour = basicDetailAnsFour;
	}
	public String getBasicDetailAnsFive() {
		return basicDetailAnsFive;
	}
	public void setBasicDetailAnsFive(String basicDetailAnsFive) {
		this.basicDetailAnsFive = basicDetailAnsFive;
	}
	public String getBasicDetailAnsSix() {
		return basicDetailAnsSix;
	}
	public void setBasicDetailAnsSix(String basicDetailAnsSix) {
		this.basicDetailAnsSix = basicDetailAnsSix;
	}
	public String getBasicDetailAnsSeven() {
		return basicDetailAnsSeven;
	}
	public void setBasicDetailAnsSeven(String basicDetailAnsSeven) {
		this.basicDetailAnsSeven = basicDetailAnsSeven;
	}
	@Override
	public String toString() {
		return "BasicDetailQuesAnsBean [besicDetail=" + basicDetail + ", basicDetailQuesFirst=" + basicDetailQuesFirst
				+ ", basicDetailQuesSecond=" + basicDetailQuesSecond + ", basicDetailQuesThird=" + basicDetailQuesThird
				+ ", basicDetailQuesFour=" + basicDetailQuesFour + ", basicDetailQuesFive=" + basicDetailQuesFive
				+ ", basicDetailQuesSix=" + basicDetailQuesSix + ", basicDetailQuesSeven=" + basicDetailQuesSeven
				+ ", basicDetailAnsFirst=" + basicDetailAnsFirst + ", basicDetailAnsSecond=" + basicDetailAnsSecond
				+ ", basicDetailAnsThird=" + basicDetailAnsThird + ", basicDetailAnsFour=" + basicDetailAnsFour
				+ ", basicDetailAnsFive=" + basicDetailAnsFive + ", basicDetailAnsSix=" + basicDetailAnsSix
				+ ", basicDetailAnsSeven=" + basicDetailAnsSeven + "]";
	}
}
