package com.mli.bot.csg.exceptions;

import com.mli.bot.csg.response.InnerData;

public class GenericCustomException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String speech;
	protected String displayText;
	protected InnerData data;

	public GenericCustomException(String speech, String displayText, InnerData data) {
		this.speech = speech;
		this.displayText = displayText;
		this.data = data;
	}

	public String getSpeech() {
		return speech;
	}

	public void setSpeech(String speech) {
		this.speech = speech;
	}

	public String getDisplayText() {
		return displayText;
	}

	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}

	public InnerData getData() {
		return data;
	}

	public void setData(InnerData data) {
		this.data = data;
	}
	
	
}
