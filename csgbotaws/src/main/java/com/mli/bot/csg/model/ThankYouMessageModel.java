package com.mli.bot.csg.model;

import java.util.Map;

public interface ThankYouMessageModel {
	public String thankYou(Map<String,Map<String,String>> externalMap, String sessionId, String action,Map map);
}
