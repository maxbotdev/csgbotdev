package com.mli.bot.csg.handlerinterfaceimpl;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.mli.bot.csg.exceptions.GenericCustomException;
import com.mli.bot.csg.exceptions.WebCustomExceptionHandler;
import com.mli.bot.csg.handlerinterface.BasicRequestBuilder;
import com.mli.bot.csg.handlerinterface.RequestResponseHandler;
import com.mli.bot.csg.request.InternalBasicRequest;
import com.mli.bot.csg.request.WebhookRequest;
import com.mli.bot.csg.response.GenericResponse;
import com.mli.bot.csg.response.WebhookResponse;
import com.mli.bot.csg.service.NeedAnalysisService;

public class WebHandler implements RequestResponseHandler {

	private static Logger logger = LogManager.getLogger(WebHandler.class);
	private WebhookRequest webReq;
	private GenericResponse webRes;
	private NeedAnalysisService needAnalysisService;
	String genericResponseDto;
	private InternalBasicRequest basicReq;
	private BasicRequestBuilder basicRequestBuilder;

	public WebHandler(WebhookRequest webReq, NeedAnalysisService needAnalysisService, BasicRequestBuilder basicRequestBuilder) {
		this.webReq = webReq;
		this.basicRequestBuilder = basicRequestBuilder;
		this.needAnalysisService = needAnalysisService;
		try 
		{
			logger.info("call request handler method start");
			requestHandler();
			logger.info("call request handler method ends");

			logger.info("call processRequest  method starts");
			processRequest();
			logger.info("call processRequest  method ends");

			logger.info("call responseHandler  method starts");
			responseHandler();
			logger.info("call responseHandler  method ends");

		} catch (GenericCustomException ex) {
		    this.webRes = new WebCustomExceptionHandler(ex).getFormattedExceptionResponse();
		}
	}

	
	@Override
	public void requestHandler() throws GenericCustomException {
		basicReq = basicRequestBuilder.getBasicRequest(webReq);
	}

	@Override
	public void responseHandler() throws GenericCustomException{
		//this.webRes = new WebhookResponse(genericResponseDto);
		Gson gson = new Gson();
		this.webRes = (GenericResponse) gson.fromJson(genericResponseDto,WebhookResponse.class);
	}

	@Override
	public GenericResponse getResponse() {

		return this.webRes;
	}

	@Override
	public void processRequest() throws GenericCustomException {
		genericResponseDto = needAnalysisService.doProcessRequest(basicReq);
	}

}
