package com.mli.bot.csg.service;

public interface CommonService {

	public byte[] convertPdfToByteArray(String source); 
	public String sendMail(String byteArray,String mailTo,String username);
	public String commaSepratedIndianFormat(String number);
	public  String camelCase(String inputString);
	
}
