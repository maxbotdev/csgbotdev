package com.mli.bot.csg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 
 * @author dipin
 *
 */
@Configuration
@EnableScheduling
@SpringBootApplication
public class CsgbotawsSpringBootApplication extends SpringBootServletInitializer {

	/**
	 * @param args
	 * Spring boot starter method
	 */
	public static void main(String[] args) {
		SpringApplication.run(CsgbotawsSpringBootApplication.class, args);
	}


	/**
	 * Used when run as WAR
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(CsgbotawsSpringBootApplication.class);
	}
}
