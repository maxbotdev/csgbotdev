package com.mli.bot.csg.serviceimpl;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mli.bot.csg.beans.ArcisServicesApiBean;
import com.mli.bot.csg.calculator.ChildEducation;
import com.mli.bot.csg.calculator.ChildMarriage;
import com.mli.bot.csg.calculator.Protectionofmyfamily;
import com.mli.bot.csg.calculator.RegularSavings;
import com.mli.bot.csg.calculator.Retirement;
import com.mli.bot.csg.commons.Adoptionlogs;
import com.mli.bot.csg.commons.BeanProperty;
import com.mli.bot.csg.commons.SessionTimeOut;
import com.mli.bot.csg.exceptions.GenericCustomException;
import com.mli.bot.csg.model.CSGQuestions;
import com.mli.bot.csg.model.FinancialProduct;
import com.mli.bot.csg.model.ProductQuestions;
import com.mli.bot.csg.model.ThankYouMessageModel;
import com.mli.bot.csg.model.UserQuestions;
import com.mli.bot.csg.request.InternalBasicRequest;
import com.mli.bot.csg.response.APIResponse;
import com.mli.bot.csg.response.CalculatorResponse;
import com.mli.bot.csg.response.FetchGoalResponse;
import com.mli.bot.csg.response.GenericNaResponse;
import com.mli.bot.csg.response.ProductDetailsResponse;
import com.mli.bot.csg.response.QuestionsResponse;
import com.mli.bot.csg.response.ThankYouResponse;
import com.mli.bot.csg.service.GetMessageService;
import com.mli.bot.csg.service.GetNonParDataService;
import com.mli.bot.csg.service.GetParDataService;
import com.mli.bot.csg.service.GetUlipDataService;
import com.mli.bot.csg.service.NeedAnalysisService;
import com.mli.bot.csg.service.PDFCreationService;
import com.mli.bot.csg.utils.ArciseServicesHelper;
import com.mli.bot.csg.utils.CommonUtility;

@Service
public class NeedAnalysisServiceImpl implements NeedAnalysisService {

	private static Logger logger = LogManager.getLogger(NeedAnalysisServiceImpl.class);	
	@Autowired
	private CSGQuestions csgQuestions;
	@Autowired
	private APIResponse apiResponse;
	@Autowired
	private FinancialProduct financialProduct;
	@Autowired
	private CalculatorResponse calculatorResponse;
	@Autowired
	private  UserQuestions userQuestions;
	@Autowired
	private  QuestionsResponse questionsResponse;
	@Autowired
	private FetchGoalResponse fetchGoalResponse;
	@Autowired
	private Protectionofmyfamily protectionofmyfamily;
	@Autowired
	private RegularSavings regularSavings;
	@Autowired
	private Retirement retirementCalculater;
	@Autowired
	private ChildMarriage childMarriageCalculater;
	@Autowired
	private ChildEducation childEducationCalculater;
	@Autowired
	private GetMessageService getMessageService;
	@Autowired
	private Adoptionlogs adoption;
	@Autowired
	GetNonParDataService getNonParDataService;
	@Autowired
	GetParDataService getParDataService;
	@Autowired
	GetUlipDataService getUlipDataService;
	@Autowired
	GenericNaResponse genericResponse;
	@Autowired
	private ProductQuestions productQuestion;
	@Autowired
	private ProductDetailsResponse productDetailsResponse;
	@Autowired
	private CommonServiceImpl cmmonServiceImpl;
	@Autowired
	private BeanProperty bean;
	@Autowired
	ThankYouMessageModel thankYouMessageModel;
	@Autowired
	private ThankYouResponse thankYouResponse;
	@Autowired
	private PDFCreationService pDFCreationService;
	@Autowired
	private SessionTimeOut sessiontimeOut;
	@Autowired
	ArciseServicesHelper arciseServicesHelper;
	@Autowired
	ArcisServicesApiBean arcisServicesApiBean;
	@Autowired
	CommonUtility commonUtility;
	
	private static Map<String, Map<String, String>> externalMap = new ConcurrentHashMap<>();
	private static Map<String, Map<String, String>> questionAswerMap = new ConcurrentHashMap<>();
	private static Map<String, String> captureSource = new ConcurrentHashMap<>();
	private static Map<String, String> questionsMap = new LinkedHashMap<>();
	private static Map<String, String> messageMap = new ConcurrentHashMap<>();
	private static Map<String, String> tokenCache = new ConcurrentHashMap<>();
	private static Map<String, Map<String, String>> response;
	private static Map basicDetailsMap = new HashMap();
    private static String sessionRemove;
    private static String childEducation;
    private static String childMarriage;
    private static String retirement;
    private static String protection;
    private static String product;
    private static String wealthAccumulation;
    private static String medium;
	

	static {
		 externalMap = new ConcurrentHashMap<>();
		 questionAswerMap = new ConcurrentHashMap<>();
		 captureSource = new ConcurrentHashMap<>();
		 questionsMap = new LinkedHashMap<>();
		 messageMap = new ConcurrentHashMap<>();
		 basicDetailsMap = new HashMap();
		 sessionRemove="sessionRemove";
		 childEducation="Child Education";
		 childMarriage="Child Marriage";
		 retirement="Retirement";
		 protection="Protection";
		 product="product";
		 wealthAccumulation= "Wealth Accumulation";
		 medium="Medium";
	}
	
	@Override
	public void removeUnUsedSessionFromCache() {
		logger.info("Cash Removed :: START---");
		sessiontimeOut.cashRemovetimeout(externalMap, questionAswerMap, response,tokenCache);
		logger.info("Cash Removed :: END---");
	}

	@Override
	public String doProcessRequest(InternalBasicRequest requestObject) throws GenericCustomException{
		Map<String, String> internalMap = new ConcurrentHashMap<>();
		String loginTime = commonUtility.getCurrentDateAndTime();

		String speech = "";
		String sessionId = "";
		String action = "";
		String questions = "";
		String queryText = "";
		try {
			
			logger.info("Request	 " + requestObject);
			sessionId = requestObject.getSession() + "";
			String sessionIdLogin = sessionId + sessionRemove;
			messageMap.put(sessionIdLogin, loginTime);
			internalMap.put(sessionIdLogin, loginTime);
			internalMap.put("start", "start");
			
			logger.info("Session Id:: :: " + sessionId + " :: login time :: " + loginTime);
			
			action =getAction(requestObject);
			queryText=getQueryText(requestObject);
			questions=queryText;
			logger.info("-----------" + action);
			setUserAns(questions,action,sessionId);
			logger.info("Action -- " + action.toUpperCase());
			inputWorry(action,sessionId);
			
			speech=csgBotActionWork(action,sessionId,questions,internalMap);

		} catch (Exception ex)
		{
			logger.error("creating exception in csg controller " + ex);
			speech = externalMap.get(sessionId + "Msg").get("ComGlitch");
		}
		return speech;
	}
	
	/**
	 * @param action is intent of API.AI
	 * @param sessionId unique id for one user in complete application
	 * @param internalMap for storing particular data for each sessionid
	 * @param questions user entered input
	 * @return speech is a outPut string which will show to user on BOT
	 */
	private String csgBotActionWork(String action, String sessionId, String questions,
			Map<String, String> internalMap)
	{
		String speech="";
		switch (action.toUpperCase())
		{
		case "INPUT.WORRY":
			speech = worryAction(sessionId,internalMap);
			break;
		case "INPUT.PROCEED":
		case "INPUT.UNKNOWN": 
			speech=proceedAction(sessionId,questions);
			break;
		case "PROCEED.PROCEED-CUSTOM": 
			 speech=proceedCustomAction(sessionId,questions);
			break;
		case "INPUT.PRODUCTDETAILS": 
			speech=productDetailsAction(sessionId,questions);
			break;
		case "INPUT.EMAIL": 
			speech=emailAction(sessionId,questions);
		break;
		case "INPUT.DIALER.CALL":
			speech=dialerCallAction(sessionId);
			break;
		default:
		speech =setDefaultMessage(sessionId);
		}
		return speech;
	}
	
	private String setDefaultMessage(String sessionId)
	{
		String speech="";
		logger.info("Intent Not match with skill, Please connect to application owner");
		speech = externalMap.get(sessionId + "Msg").get("Default");
		return speech;
	}
	
	private String dialerCallAction(String sessionId)
	{
		logger.info("Start dialerCallAction ....");
		String speech="";
		if (externalMap.containsKey(sessionId + sessionRemove))
		{
			
			speech=callArciseServices(sessionId,"ClickCallToButton");
			
			speech = thankYouResponse.thankyouResponse(speech);
		}else 
		{
			speech = bean.getSessionTimeoutMessage();
			speech = thankYouResponse.thankyouResponse(speech);
		}
		logger.info("End dialerCallAction ...."+speech);
		return speech;
	}
	
	private String emailAction(String sessionId, String questions) {
		String speech="";
		
		if (externalMap.containsKey(sessionId + sessionRemove))
		{
			logger.info("Inside Email intent");
			speech = thankYouMessageModel.thankYou(externalMap, sessionId, questions, questionAswerMap);
			if (!"".equalsIgnoreCase(speech) && speech != null) {
				Map<String, String> newMap = questionAswerMap.get(sessionId);
				for (Entry<String, String> DyanamicResponse : newMap.entrySet()) {
					String dyanmickey = DyanamicResponse.getKey();
					String dyanamicValue = DyanamicResponse.getValue();
					if (speech.contains("<" + dyanmickey + ">")) {
						speech = speech.replace("<" + dyanmickey + ">", dyanamicValue);
						break;
					}
				}
			}
			speech = thankYouResponse.thankyouResponse(speech);
		} else {
			speech = bean.getSessionTimeoutMessage();
			speech = thankYouResponse.thankyouResponse(speech);
		}
		
		return speech;
	}

	
	private String productDetailsAction(String sessionId, String questions) {
		String speech="";
		
		if (externalMap.containsKey(sessionId + sessionRemove)) {
			logger.info(product+" details intent call ");
			String username = "";
			try {
				if (!"".equalsIgnoreCase(speech))
				{
					Map<String, String> newMap = questionAswerMap.get(sessionId);

					for (Entry<String, String> DyanamicResponse : newMap.entrySet())
					{
						String dyanmickey = DyanamicResponse.getKey();
						String dyanamicValue = DyanamicResponse.getValue();

						if ("Q27".equalsIgnoreCase(dyanmickey))
						{
							username = cmmonServiceImpl.camelCase(dyanamicValue);
							break;
						}
					}
				}

				String fileName = sessionId.split("/")[4] + ".pdf";

				String workingDir = bean.getGetPdfPath();
				logger.info("Working dir ++++ " + workingDir);
				logger.info("Path ==== " + workingDir + File.separator + fileName);
				
		   pDFCreationService.createPdf(username, workingDir + File.separator + fileName, externalMap, sessionId,questions);
		   
			} catch (Exception e)
			{
				logger.error("creating exception while creating pdf " + e);
			}
			productQuestion.productQuestions(externalMap, sessionId, questions);
			speech = productDetailsResponse.productResponse(externalMap, sessionId);
			logger.info(product+" details intent call End");
		} else {
			speech = bean.getSessionTimeoutMessage();
			speech = thankYouResponse.thankyouResponse(speech);
		}
	
		return speech;
	}
	
	
	private String proceedCustomAction( String sessionId, String questions) {
		String speech="";
		
		if (externalMap.containsKey(sessionId + sessionRemove))
		{
			
			
			String action = "Questions";
			logger.info("PROCEED.PROCEED-CUSTOM intent call.. ");
			
			
			if (!"Exists".equalsIgnoreCase(externalMap.get(sessionId).get("Identifier")))
			{
				speech=getQuestions(sessionId,action,questions);
				Map<String,String> planName=new HashMap();
				
				planName.put("plan",questions.split("answer")[1].replace("$$", ""));
				externalMap.put(sessionId+"planName",planName);

			} else
			{
				speech=getExistsQuestion(sessionId);
			}
		} else {
			speech = bean.getSessionTimeoutMessage();
			speech = thankYouResponse.thankyouResponse(speech);
		}
		logger.info("calculatorResponse " + speech);
		return speech;
	}
	
	private String getExistsQuestion(String sessionId) {

		String speech="";
		
		
		LinkedHashMap<String, String> linkedQuestions = (LinkedHashMap<String, String>) response
				.get(sessionId);
		logger.info("linkedQuestions in else condition..." + linkedQuestions);
		
		if (!linkedQuestions.isEmpty())
		{
			for (Entry<String, String> entry : linkedQuestions.entrySet())
			{
				String key = entry.getKey();
				String value = entry.getValue();
				questionsMap.put(key, "");
				questionAswerMap.put(sessionId, questionsMap);
					logger.info("Sesssion Id " + sessionId);
					Map<String, String> newMap = questionAswerMap.get(sessionId);

					for (Entry<String, String> DyanamicResponse : newMap.entrySet())
					{

						String dyanmickey = DyanamicResponse.getKey();
						String dyanamicValue = DyanamicResponse.getValue();
						logger.info("Sesssion Id " + sessionId + " Key " + dyanmickey + " value "
								+ dyanamicValue);
						if (value.contains("<" + dyanmickey + ">"))
						{
							value = value.replace("<" + dyanmickey + ">", dyanamicValue);
							break;
						}
					}
						logger.info(sessionId + "next question for user " + value);
						speech = questionsResponse.questionsResponse( sessionId, value, key,
							questionAswerMap);
						logger.info("speech for new question " + speech);
						
						response.get(sessionId).remove(key);
						break;
							
			     }
		} else 
		{
			speech=existQuestionNotFound(sessionId);
			speech = calculatorResponse.calculatorResponse(externalMap, sessionId, speech);
		}
	return speech;
	}
	

	private String existQuestionNotFound(String sessionId) {
		
        String speech="";
       
		String pocket = "";
		String  term = "";
		String risk = "";
		String age = "";
		
		 String responsefromCalc = externalMap.get(sessionId).get("Goals");
		 String[] splitResponse = responsefromCalc.split(",");

		 Map<String,String> quesMap=calculateAgeAndTeam(sessionId,splitResponse[0]);
		  if(quesMap!=null)
		  {
		     age=quesMap.get("age");
		     risk= quesMap.get("risk");
		     pocket=quesMap.get("pocket" );
	         term= quesMap.get("term");
		 }
		logger.info("age- " + age + "  term- " + term + "  pocket-  " + pocket+ "risk "+risk);
			
		
		
		for (int i = 0; i < splitResponse.length; i++)
		{
			if ("PF".equalsIgnoreCase(splitResponse[i]))
			{
				logger.info("Inside PF ");
				String nonPerResponse = "";
				String finalProductResponse = "";
				try
				{
					logger.info("any risk calling NonPer " + risk);

					nonPerResponse = getNonParDataService.getNonPerData(age, term, pocket,
							protection);

				} catch (Exception e)
				{
					logger.error("creating    exception while calling risk in protectionofmyfamily!! " + e);
				}

				if (nonPerResponse != null && !nonPerResponse.equals(""))
				{
					String[] nonPerActualResponse = nonPerResponse.split("\\,");
					finalProductResponse = ","+product+"=" + nonPerActualResponse[0] + "";
				}
				logger.info("Final  Response for protection my family :: " + finalProductResponse);
				try
				{
					logger.info("start ... protectionofmyfamily response - ");
					String responseprotectionofmyfamily = protectionofmyfamily
							.protectionOfMyFamily(questionAswerMap, sessionId);
					externalMap.get(sessionId).put("protectionofmyfamily",
							responseprotectionofmyfamily + finalProductResponse);
					logger.info("End ... protectionofmyfamily response - "
							+ responseprotectionofmyfamily + finalProductResponse);
				} catch (Exception e)
				{
					logger.error("creating exception while making response for protectionofmyfamily "+ e);
				}
			} else if ("RS".equalsIgnoreCase(splitResponse[i]))
			{
				logger.info("Inside RS Calculater");
				String nonPerResponse = "";
				String finalProductResponse = "";
				try
				{
					if ("Low".equalsIgnoreCase(risk))
					{
						logger.info("Low ..   Risk ..    Calling..."+wealthAccumulation+"" + risk);
						nonPerResponse = getNonParDataService.getNonPerData(age, term, pocket,
								wealthAccumulation);

					} else if ("High".equalsIgnoreCase(risk))
					{
						logger.info("High Risk     Calling..."+wealthAccumulation+"" + risk);
						nonPerResponse = getUlipDataService.getUlipData(age, term, pocket,
								wealthAccumulation);

					} else if (medium.equalsIgnoreCase(risk))
					{
						logger.info(medium+" Risk      Calling..."+wealthAccumulation+"" + risk);
						nonPerResponse = getParDataService.getParData(age, term, pocket,
								wealthAccumulation);
					}
				} catch (Exception e) 
				{
					logger.error("creating exception   while calling   risk in "+wealthAccumulation+"!! " + e);
				}

				if (nonPerResponse != null && !nonPerResponse.equals(""))
				{
					String[] nonPerActualResponse = nonPerResponse.split("\\,");
					finalProductResponse = ","+product+"=" + nonPerActualResponse[0] + "";
				}

				logger.info("Final  Response for regular saving :: " + finalProductResponse);
				try
				{
					String responseRegularSaving = regularSavings.regularSavings(questionAswerMap,
							sessionId);
					externalMap.get(sessionId).put("RegularSaving",
							responseRegularSaving + finalProductResponse);
					logger.info("RegularSaving", responseRegularSaving + finalProductResponse);
				} catch (Exception e) 
				{
					logger.error("creating exception while making response for RegularSaving " + e);
				}
			}
			else if ("ER".equalsIgnoreCase(splitResponse[i]))
			{
				logger.info("Inside ER ");
				String nonPerResponse = "";
				String finalProductResponse = "";
				try {
					if ("Low".equalsIgnoreCase(risk)) {
						logger.info("Low   Risk. Calling..."+retirement+" " + risk);

						nonPerResponse = getNonParDataService.getNonPerData(age, term, pocket,
								retirement);

					} else if ("High".equalsIgnoreCase(risk)) {
						logger.info("High Risk..  Calling..."+retirement+" " + risk);
						nonPerResponse = getUlipDataService.getUlipData(age, term, pocket,
								retirement);

					} else if (medium.equalsIgnoreCase(risk)) {
						logger.info(medium+" Risk ......Calling....."+retirement+" " + risk);
						nonPerResponse = getParDataService.getParData(age, term, pocket,
								retirement);
					}
				} catch (Exception e)
				{
					logger.error("  creating exception while calling risk   in "+retirement+"!! " + e);
				}

				if (nonPerResponse != null && !nonPerResponse.equals(""))
				{
					String[] nonPerActualResponse = nonPerResponse.split("\\,");
					finalProductResponse = ","+product+"=" + nonPerActualResponse[0] + "";
				}

				logger.info("Final  Response retirement:: " + finalProductResponse);
				try
				{
					String responseRetirement = retirementCalculater.retirement(questionAswerMap, sessionId);
					externalMap.get(sessionId).put(retirement,
							responseRetirement + finalProductResponse);
					logger.info(retirement, responseRetirement + finalProductResponse);
				} catch (Exception e)
				{
					logger.error("creating exception while making response for  "+retirement+" " + e);
				}
			} else if ("CE".equalsIgnoreCase(splitResponse[i]))
			{
				logger.info("Inside CE ");
				String nonPerResponse = "";
				String finalProductResponse = "";
				try
				{
					if ("Low".equalsIgnoreCase(risk))
					{
						logger.info("Low Risk   Calling..."+childEducation+"" + risk);
						nonPerResponse = getNonParDataService.getNonPerData(age, term, pocket,
								childEducation);

					} else if ("High".equalsIgnoreCase(risk))
					{
						logger.info("High Risk    Calling..."+childEducation+" " + risk);
						nonPerResponse = getUlipDataService.getUlipData(age, term, pocket,
								childEducation);

					} else if (medium.equalsIgnoreCase(risk))
					{
						logger.info(medium+" Risk ..."+childEducation+" " + risk);
						nonPerResponse = getParDataService.getParData(age, term, pocket,
								childEducation);
					}
				} catch (Exception e)
				{
					logger.error("creating exception while   calling risk in ChildEducation!! " + e);
				}

				if (nonPerResponse != null && !nonPerResponse.equals("")) {
					String[] nonPerActualResponse = nonPerResponse.split("\\,");
					finalProductResponse = ","+product+"=" + nonPerActualResponse[0] + "";
				}

				logger.info("Final  Response  Child Education  :: " + finalProductResponse);
				try
				{
					String responsechildEducation = childEducationCalculater.childEducation(questionAswerMap,
							sessionId);
					externalMap.get(sessionId).put("ChildEducation",
							responsechildEducation + finalProductResponse);
					logger.info("ChildEducation", responsechildEducation + finalProductResponse);
				} catch (Exception e)
				{
					logger.error("creating exception while making response for ChildEducation " + e);
				}
			} else if ("CM".equalsIgnoreCase(splitResponse[i]))
			{
				logger.info("Inside CM ");
				String nonPerResponse = "";
				String finalProductResponse = "";
				try
				{
					if ("Low".equalsIgnoreCase(risk))
					{
						logger.info("Low  Risk ...Calling..."+childMarriage+" " + risk);
						nonPerResponse = getNonParDataService.getNonPerData(age, term, pocket,
								childMarriage);

					} else if ("High".equalsIgnoreCase(risk))
					{
						logger.info("High  Risk .....Calling..."+childMarriage+" " + risk);
						nonPerResponse = getUlipDataService.getUlipData(age, term, pocket,
								childMarriage);

					} else if (medium.equalsIgnoreCase(risk))
					{
						logger.info(medium+" Risk ....Calling..."+childMarriage+" " + risk);
						nonPerResponse = getParDataService.getParData(age, term, pocket,
								childMarriage);
					}
				} catch (Exception e)
				{
					logger.error("creating  exception   while calling risk in "+childMarriage+" !! " + e);
				}

				if (nonPerResponse != null && !nonPerResponse.equals(""))
				{
					String[] nonPerActualResponse = nonPerResponse.split("\\,");
					finalProductResponse = ","+product+"=" + nonPerActualResponse[0] + "";
				}

				logger.info("Final  Response for child marriage :: " + finalProductResponse);
				try
				{
					String responseChildMarriage = childMarriageCalculater.childMarriage(questionAswerMap,
							sessionId);
					externalMap.get(sessionId).put("ChildMarriage",
							responseChildMarriage + finalProductResponse);
					logger.info("ChildMarriage", responseChildMarriage + finalProductResponse);
				} catch (Exception e)
				{
					logger.error("creating exception while making response for ChildMarriage " + e);
				}
			}
		}
		
		
		
		
		basicDetailsMap.put(sessionId + "QuestionAswerMap", questionAswerMap);
		basicDetailsMap.put(sessionId + "term", term);
		externalMap.put(sessionId + "basicDetailsMap", basicDetailsMap);

		speech = externalMap.get(sessionId + "Msg").get("FinalMSG");
		

		if (!"".equalsIgnoreCase(speech) && speech != null)
		{
			Map<String, String> newMap = questionAswerMap.get(sessionId);

			for (Entry<String, String> DyanamicResponse : newMap.entrySet())
			{
				String dyanmickey = DyanamicResponse.getKey();
				String dyanamicValue = DyanamicResponse.getValue();

				if ("Q27".equalsIgnoreCase(dyanmickey)) {
					cmmonServiceImpl.camelCase(dyanamicValue);
				}

				if (speech.contains("<" + dyanmickey + ">")) {
					speech = speech.replace("<" + dyanmickey + ">", dyanamicValue);
					break;
				}
			}
		}
		

		return speech;
	}
	
	private Map<String,String> calculateAgeAndTeam(String sessionId,String splitResponse) {
		String pocket = "";
		String  term = "";
		String risk = "";
		String age = "";
		 String age1 = "";
		 String age2 = "";
		Map<String,String> listMap=null;
		
		try 
		{
			listMap=new HashMap();
		for (Entry<String, String> entry : questionAswerMap.get(sessionId).entrySet())
		{
			String key = entry.getKey();
			String value = entry.getValue();

			if ("Q10".equalsIgnoreCase(key)) {
				age1 = value;
			}
			if ("Q2".equalsIgnoreCase(key)) {
				age2 = value;
			}
			if ("Q7".equalsIgnoreCase(key)) {
				age1 = value;
			}
			if ("2".equalsIgnoreCase(key)) {
				age2 = value;
			}
			if ("Q19".equalsIgnoreCase(key)) {
				age1 = value;
			}
			if ("Q13".equalsIgnoreCase(key)) {
				age2 = value;
			}
			if ("Q21".equalsIgnoreCase(key)) {
				age1 = value;
			}
			if ("Q13".equalsIgnoreCase(key)) {
				age2 = value;
			}
			if ("Q13".equalsIgnoreCase(key)) {
				age2 = value;
			}
			if ("Q13".equalsIgnoreCase(key)) {
				age = value;
			} else if ("Q25".equalsIgnoreCase(key)) {
				pocket = value;
			} else if ("Q26".equalsIgnoreCase(key)) {
				risk = value;
			}

		}
	term=getTerm(age,splitResponse,age1,age2);
	
	listMap.put("age",age);
	listMap.put("risk",risk);
	listMap.put("pocket",pocket );
	listMap.put("term",term);
	
		}catch(Exception e)
		{
		 logger.error("creating exception in calculateAgeAndTeam "+e);
		}
	
	return listMap;
	}
	
	private String getTerm(String age,String splict, String age1, String age2)
	{
		String term="";
		try
		{
			if ("PF".equalsIgnoreCase(splict)) {
			term=String.valueOf((85 - Integer.parseInt(age)));	
			}else {
				term = String.valueOf(Integer.parseInt(age1) - Integer.parseInt(age2));
			}
			
			
			
		} catch (Exception e)
		{
			logger.error("creating exception while calculating term " + e);
		}
		return term;
	}
	
	
	public  String getQuestions(String sessionId,String action, String questions) {

		String speech="";
		
		if ("google".equalsIgnoreCase(captureSource.get(sessionId) + ""))
		{
			questions = protection;
			response = userQuestions.userQuestions(externalMap, sessionId, action, questions);
		} else
		{
			response = userQuestions.userQuestions(externalMap, sessionId, action, questions);
			logger.info("Question Response :: " + response);
		}
		
		
		LinkedHashMap<String, String> linkedQuestions = (LinkedHashMap<String, String>) response
				.get(sessionId);
		
				for (Entry<String, String> entry : linkedQuestions.entrySet())
				{
					logger.info("linkedQuestions in if condition..." + linkedQuestions);
					String key = entry.getKey();
					String value = entry.getValue();
					questionsMap = new LinkedHashMap<>();
					questionsMap.put(key, "");
					questionAswerMap.put(sessionId, questionsMap);
					
						speech = questionsResponse.questionsResponse( sessionId, value, key,
								questionAswerMap);
						response.get(sessionId).remove(key);
						break;
					
				}
		return speech;
	}
	
	private String proceedAction(String sessionId, String questions)
	{
		String speech="";
		if (externalMap.containsKey(sessionId + sessionRemove))
		{
				String action = "Proceed";
				speech = financialProduct.financialProduct(externalMap, sessionId, action, questions);
				speech = fetchGoalResponse.fetchGoalResponse(externalMap, sessionId, speech);
		
		} else {
			speech = bean.getSessionTimeoutMessage();
			speech = thankYouResponse.thankyouResponse(speech);
		}
		
		return speech;
	}

	private String  worryAction(String sessionId, 
			Map<String, String> internalMap)
	{
		String speech="";
		
		externalMap.put(sessionId + sessionRemove, internalMap);
		if (externalMap.containsKey(sessionId + sessionRemove))
		{
			logger.info("Input worry intent call.. ");
			
				String action = "Worry";
				speech = csgQuestions.csgQuestions(externalMap, sessionId, action);
				speech = apiResponse.apiResponse(externalMap, sessionId, speech);
			
		} else
		{
			speech = bean.getSessionTimeoutMessage();
			speech = thankYouResponse.thankyouResponse(speech);
		}
		return speech;
	}
	
	private void inputWorry(String action,String sessionId) {
		if ("INPUT.WORRY".equalsIgnoreCase(action))
		{
			try 
			{
				String textMessage = getMessageService.getCsgMessages("csgbot");
				JSONArray jsonArr = new JSONArray(textMessage);
				for (int i = 0; i < jsonArr.length(); i++)
				{
					String result = jsonArr.get(i) + "";
					String[] arr = result.split("###");
					messageMap.put(arr[0], arr[1]);
				}
				externalMap.put(sessionId + "Msg", messageMap);

			} catch (Exception e)
			{
				logger.info("creating exception while geting questions from rule engine " + e);
			}
		}	
	}
	
	private static void setUserAns(String questions,String action, String sessionId) {
		try 
		{
			if ("proceed.proceed-custom".equalsIgnoreCase(action) && externalMap.containsKey(sessionId) && questionsMap != null)
			{
					questionsMap = questionAswerMap.get(sessionId);
						for (Entry<String, String> entry : questionsMap.entrySet()) {
							String key = entry.getKey();
							String value = entry.getValue();
							if ("".equalsIgnoreCase(value)) {
								String[] strSplit = questions.split("\\$\\$");
								questionsMap.put(key, strSplit[1]);
								questionAswerMap.put(sessionId, questionsMap);
							}
						}
					
				
			}
			logger.info("---Questions1--------" + questions);
		} catch (Exception ex) {
			logger.error("creating Exception while setting the user ans in question map "+ex);
		}
	}

	private String getQueryText(InternalBasicRequest object)
	{
		String queryText="";
		try {
			if(object.getQueryText()!=null&&!object.getQueryText().isEmpty()) {
				queryText = object.getQueryText().toString();				
			}
		} catch (Exception ex) {
			queryText = "";
		}
		return queryText;
	}
	
	private String getAction(InternalBasicRequest object) 
	{
		String action="";
		try {
		if(object.getAction()!=null&&!object.getAction().isEmpty()) {
			action = object.getAction().toString();
		}
		}catch(Exception e) {
			logger.error("Creating exception in getAction"+e);
		}
		return action;
	} 
	@Override
	public String getArciseToken() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String callArciseServices(String sessionId, String sourceName) {
		// TODO Auto-generated method stub
		return null;
	}

}
