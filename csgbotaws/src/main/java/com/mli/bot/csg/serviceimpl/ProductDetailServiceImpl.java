package com.mli.bot.csg.serviceimpl;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mli.bot.csg.commons.BeanProperty;
import com.mli.bot.csg.service.ProductDetails;
@Service
public class ProductDetailServiceImpl implements ProductDetails{
	@Autowired
	private BeanProperty bean;
	private static Logger logger = LogManager.getLogger(ProductDetailServiceImpl.class);
	String questionsResponse;
	@Override
	public String productdetailsQuestion(Map<String, Map<String, String>> externalMap, String sessionId,
			String product) {
		
		String url=bean.getGetProductDetail();
		logger.info("Start method ....productdetailsQuestion ");
		logger.info("URL ::"+url);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"Input\":\""+product+"\"	");
		sb.append("	}	");

		HttpEntity<String> entity=new HttpEntity<>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		
		logger.info("Product Response :: "+response);
		if(response.getStatusCodeValue() == 200)
		{
			questionsResponse = response.getBody();
			logger.info("---ProductDetails-----"+questionsResponse);

		}logger.info("End method ....productdetailsQuestion");
		return questionsResponse;
	}
	}


